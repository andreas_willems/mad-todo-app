package de.andreaswillems.android.mad_todoapp_v2.network;

import de.andreaswillems.android.mad_todoapp_v3.connection.NetworkConnection;

public class NetworkConfig {
    public final static String HOST_ADDRESS = NetworkConnection.HOST_ADDRESS; // "http://192.168.1.121:8080";
}
