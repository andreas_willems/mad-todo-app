package de.andreaswillems.android.mad_todoapp_v2.network;

import java.io.IOException;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.Credentials;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static de.andreaswillems.android.mad_todoapp_v2.network.NetworkConfig.HOST_ADDRESS;


public class LoginRestOperationsImpl {

    private LoginRestOperations mWebApi;

    public LoginRestOperationsImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mWebApi = retrofit.create(LoginRestOperations.class);
    }

    public boolean requestLogin(Credentials credentials) {
        boolean success = false;
        try {
            success = this.mWebApi.requestLogin(credentials).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
}
