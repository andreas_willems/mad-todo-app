package de.andreaswillems.android.mad_todoapp_v2.mapping_entities;


import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataItem {
    private long id;
    private String name;
    private String description;
    private long expiry;
    private boolean done;
    private boolean favourite;
    private List<String> contacts;
    private String location;


    public DataItem(long id,
                    String name,
                    String description,
                    long expiry,
                    boolean done,
                    boolean favourite,
                    List<String> contacts,
                    String location) {
        this(name, description, expiry, done, favourite, contacts, location);
        this.id = id;
    }

    public DataItem(String name,
                    String description,
                    long expiry,
                    boolean done,
                    boolean favourite,
                    List<String> contacts,
                    String location) {
        this.name = name;
        this.description = description;
        this.expiry = expiry;
        this.done = done;
        this.favourite = favourite;
        this.contacts = contacts;
        this.location = location;
    }

    public DataItem(long id,
                    String name,
                    String description,
                    long expiry,
                    boolean done,
                    boolean favourite,
                    String contacts,
                    String location) {
        this(name, description, expiry, done, favourite, new ArrayList<String>(), location);
        this.id = id;
        this.contacts = asListOfStrings(contacts);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> asListOfStrings(String contactsCSV) {
        List<String> list = new ArrayList<>();
        if (null != contactsCSV) {
            String[] parts = contactsCSV.split(";");
            Collections.addAll(list, parts);
        }
        return list;
    }
}
