package de.andreaswillems.android.mad_todoapp_v2.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


class DataItemDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "AWTodoApp.db";

    DataItemDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTableStatement());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(dropTableStatement());
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    private String createTableStatement() {
        String tableName = DataItemContract.DataItemEntry.TABLE_NAME;
        String id = DataItemContract.DataItemEntry._ID;
        String name = DataItemContract.DataItemEntry.COLUMN_NAME;
        String description = DataItemContract.DataItemEntry.COLUMN_DESCRIPTION;
        String expiry = DataItemContract.DataItemEntry.COLUMN_EXPIRY;
        String done = DataItemContract.DataItemEntry.COLUMN_DONE;
        String favorite = DataItemContract.DataItemEntry.COLUMN_FAVORITE;
        String contacts = DataItemContract.DataItemEntry.COLUMN_CONTACTS;
        String location = DataItemContract.DataItemEntry.COLUMN_LOCATION;

        return "CREATE TABLE IF NOT EXISTS " + tableName
                + " (" + id + " INTEGER PRIMARY KEY,"
                + name + " TEXT,"
                + description + " TEXT,"
                + expiry + " INTEGER,"
                + done + " BOOLEAN,"
                + favorite + " BOOLEAN,"
                + contacts + " TEXT,"
                + location + " TEXT)";
    }

    private String dropTableStatement() {
        return "DROP TABLE IF EXISTS "
                + DataItemContract.DataItemEntry.TABLE_NAME;
    }
}
