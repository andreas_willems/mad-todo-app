package de.andreaswillems.android.mad_todoapp_v2.storage;


import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.DataItem;

public interface ICRUDOperations {

    public long create(String name,
                       String description,
                       long expiry,
                       boolean isDone,
                       boolean isFavorite,
                       String contacts,
                       String location);

    public DataItem read(long id);

    public List<DataItem> readAll();

    public boolean update(long id,
                          String name,
                          String description,
                          long expiry,
                          boolean isDone,
                          boolean isFavorite,
                          String contacts,
                          String location);

    public boolean delete(long id);

    public int itemCount();

    public boolean deleteAll();
}
