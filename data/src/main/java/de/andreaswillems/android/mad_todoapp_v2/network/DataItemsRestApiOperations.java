package de.andreaswillems.android.mad_todoapp_v2.network;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.DataItem;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Defines the HTTP operations to provide CRUD operations against the web api.
 *
 * @author Andreas Willems
 * @version 28 JUN 2017
 */

public interface DataItemsRestApiOperations {

    @POST("/api/todos")
    public Call<DataItem> createDataItem(@Body DataItem todoItem);

    @GET("/api/todos")
    public Call<List<DataItem>> readAllDataItems();

    @GET("/api/todos/{id}")
    public Call<DataItem> readDataItem(@Path("id") long id);

    @PUT("/api/todos/{id}")
    public Call<DataItem> updateDataItem(@Path("id") long id, @Body DataItem dataItem);

    @DELETE("/api/todos/{id}")
    public Call<Boolean> deleteDataItem(@Path("id") long id);

    @DELETE("/api/todos")
    public Call<Boolean> deleteAllItems();
}
