package de.andreaswillems.android.mad_todoapp_v2.network;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.DataItem;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static de.andreaswillems.android.mad_todoapp_v2.network.NetworkConfig.HOST_ADDRESS;

/**
 * Implements the operations / request against the REST api.
 *
 * @author Andreas Willems
 * @version 10 JUN 2017
 */

public class DataItemsRestApiOperationsImpl {

    private DataItemsRestApiOperations mWebApi;

    public DataItemsRestApiOperationsImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mWebApi = retrofit.create(DataItemsRestApiOperations.class);
    }

    /**
     * Creates a new data item at the webservice using a POST-request.
     */
    public DataItem createDataItem(DataItem item) {
        DataItem dataItem = null;
        try {
            dataItem = this.mWebApi.createDataItem(item).execute().body();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return dataItem;
    }

    /**
     * Reads all items stored in the webservice using a GET-request.
     */
    public List<DataItem> readAllDataItems() {
        try {
            return this.mWebApi.readAllDataItems().execute().body();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Reads a single dataitem from the webservice using a GET-request and the given id.
     */
    public DataItem readDataItem(long id) {
        DataItem dataItem = null;
        try {
            dataItem = this.mWebApi.readDataItem(id).execute().body();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return dataItem;
    }

    /**
     * Updates the given item at the webservice using a PUT-request.
     */
    public boolean updateDataItem(long id, DataItem dataItem) {
        Log.i("remote request", dataItem.getContacts().toString());
        boolean success = true;
        DataItem returnedItem;
        try {
            // success = this.mWebApi.updateDataItem(id, dataItem).execute().isSuccessful();
            returnedItem = this.mWebApi.updateDataItem(id, dataItem).execute().body();
            Log.i("remote update", returnedItem.getContacts().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return success;
    }

    /**
     * Sends a DELETE-request to a specialized endpoint to delete all items in the webservice.
     */
    public boolean deleteAllItems() {
        try {
            return this.mWebApi.deleteAllItems().execute().isSuccessful();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deletes the dataitem with the given id using a DELETE-request.
     */
    public boolean deleteDataItem(long id) {
        boolean success = false;
        try {
            success = this.mWebApi.deleteDataItem(id).execute().isSuccessful();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return success;
    }
}
