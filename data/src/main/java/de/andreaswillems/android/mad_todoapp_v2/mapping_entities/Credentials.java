package de.andreaswillems.android.mad_todoapp_v2.mapping_entities;


import java.util.HashMap;

public class Credentials {

    private String email;
    private String pwd;


    public Credentials(String email, String password) {
        this.email = email;
        this.pwd = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
