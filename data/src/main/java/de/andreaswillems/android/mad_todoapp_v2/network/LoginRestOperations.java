package de.andreaswillems.android.mad_todoapp_v2.network;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.Credentials;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

/**
 * Defines the HTTP operations to provide CRUD operations against the web api.
 *
 * @author Andreas Willems
 * @version 29 JUN 2017
 */

public interface LoginRestOperations {

    @PUT("/api/users/auth")
    public Call<Boolean> requestLogin(@Body Credentials credentials);
}
