package de.andreaswillems.android.mad_todoapp_v2.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.DataItem;


public class CRUDOperationsImpl implements ICRUDOperations {

    private static String tableName = DataItemContract.DataItemEntry.TABLE_NAME;
    private static String idKey = DataItemContract.DataItemEntry._ID;
    private static String nameKey = DataItemContract.DataItemEntry.COLUMN_NAME;
    private static String descriptionKey = DataItemContract.DataItemEntry.COLUMN_DESCRIPTION;
    private static String expiryKey = DataItemContract.DataItemEntry.COLUMN_EXPIRY;
    private static String doneKey = DataItemContract.DataItemEntry.COLUMN_DONE;
    private static String favoriteKey = DataItemContract.DataItemEntry.COLUMN_FAVORITE;
    private static String contactsKey = DataItemContract.DataItemEntry.COLUMN_CONTACTS;
    private static String locationKey = DataItemContract.DataItemEntry.COLUMN_LOCATION;
    private SQLiteDatabase database;
    private DataItemDbHelper mDbHelper;

    public CRUDOperationsImpl(Context context) {
        mDbHelper = new DataItemDbHelper(context);
        database = mDbHelper.getWritableDatabase();
    }

    @Override
    public long create(String name,
                       String description,
                       long expiry,
                       boolean isDone,
                       boolean isFavorite,
                       String contacts,
                       String location) {
        ContentValues values = new ContentValues();
        values.put(nameKey, name);
        values.put(descriptionKey, description);
        values.put(expiryKey, expiry);
        values.put(doneKey, Boolean.toString(isDone));
        values.put(favoriteKey, Boolean.toString(isFavorite));
        values.put(contactsKey, contacts);
        values.put(locationKey, location);

        return database.insert(tableName, null, values);
    }

    @Override
    public DataItem read(long id) {
        String[] projection = {
                idKey, nameKey, descriptionKey, expiryKey, doneKey,
                favoriteKey, contactsKey,locationKey
        };
        String selection = idKey + " = ?";
        String[] selectionArgs = { String.valueOf(id) };
        Cursor cursor = database.query(tableName, projection, selection,
                selectionArgs, null, null, null);
        List<DataItem> result = fetchDataFromCursor(cursor);
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }

    }

    public List<DataItem> readAll() {
        String[] projection = {
            idKey, nameKey, descriptionKey, expiryKey, doneKey, favoriteKey,
                contactsKey,locationKey
        };
        String selection = null;
        String[] selectonArgs = null;
        String sortOrder = idKey + " ASC";

        Cursor cursor = database.query(
                tableName,
                projection,
                selection,
                selectonArgs,
                null,
                null,
                sortOrder
        );

        List<DataItem> dataItems = fetchDataFromCursor(cursor);
        cursor.close();
        return dataItems;
    }

    private List<DataItem> fetchDataFromCursor(Cursor cursor) {
        List<DataItem> rows = new ArrayList<>();
        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndex(idKey));
            String name =
                    cursor.getString(cursor.getColumnIndex(nameKey));
            String description =
                    cursor.getString(cursor.getColumnIndex(descriptionKey));
            long expiry =
                    cursor.getLong(cursor.getColumnIndex(expiryKey));
            boolean done =
                    Boolean.parseBoolean(cursor.getString(
                            cursor.getColumnIndex(doneKey)));
            boolean favorite =
                    Boolean.parseBoolean(cursor.getString(
                            cursor.getColumnIndex(favoriteKey)));
            String contacts=
                    cursor.getString(cursor.getColumnIndex(contactsKey));
            String location =
                    cursor.getString(cursor.getColumnIndex(locationKey));
            rows.add(new DataItem(
                    id,
                    name,
                    description,
                    expiry,
                    done,
                    favorite,
                    contacts,
                    location
            ));
        }
        return rows;
    }

    @Override
    public boolean update(long id,
                          String name,
                          String description,
                          long expiry,
                          boolean isDone,
                          boolean isFavorite,
                          String contacts,
                          String location) {

        ContentValues values = new ContentValues();
        values.put(nameKey, name);
        values.put(descriptionKey, description);
        values.put(expiryKey, expiry);
        values.put(doneKey, Boolean.toString(isDone));
        values.put(favoriteKey, Boolean.toString(isFavorite));
        values.put(contactsKey, contacts);
        values.put(locationKey, location);

        // Which row to update, based on the title
        String selection = idKey + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        int count = database.update(tableName, values, selection, selectionArgs);
        return count > 0;
    }

    @Override
    public boolean delete(long id) {
        String selection = idKey + " = ?";
        String[] selectionArgs = { String.valueOf(id) };
        int count = database.delete(tableName, selection, selectionArgs);
        return count > 0;
    }

    @Override
    public int itemCount() {
        String[] projection = { idKey };
        Cursor cursor = database.query(
                tableName, projection, null, null, null, null, null
        );
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    @Override
    public boolean deleteAll() {
        String selection = idKey + " > ?";
        String[] selectionArgs = { String.valueOf(-1) };
        int count = database.delete(tableName, selection, selectionArgs);
        return count > 0;
    }
}
