package de.andreaswillems.android.mad_todoapp_v2.storage;


import android.provider.BaseColumns;

class DataItemContract {

    // prohibit instantiation
    private DataItemContract() {}

    // define schema
    static class DataItemEntry implements BaseColumns {
        static final String TABLE_NAME = "dataItemEntry";
        static final String COLUMN_NAME = "name";
        static final String COLUMN_DESCRIPTION = "description";
        static final String COLUMN_EXPIRY = "expiry";
        static final String COLUMN_DONE = "done";
        static final String COLUMN_FAVORITE = "favorite";
        static final String COLUMN_CONTACTS = "contacts";
        static final String COLUMN_LOCATION = "location";
    }
}
