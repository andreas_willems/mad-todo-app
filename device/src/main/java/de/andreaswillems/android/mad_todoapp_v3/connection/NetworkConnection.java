package de.andreaswillems.android.mad_todoapp_v3.connection;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class NetworkConnection {

    public final static String HOST_ADDRESS = "http://192.168.1.121:8080";
    private final static int READ_TIMEOUT = 1000;
    private final static int CONNECT_TIMEOUT = 2000;

    private Context context;

    public NetworkConnection(Context context) {
        this.context = context;
    }

    public boolean isConnectedToNetwork() {
        ConnectivityManager cm =
                (ConnectivityManager) this.context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null &&
                networkInfo.isConnectedOrConnecting();
    }

    public boolean isWebserviceAvailable() {
        boolean available = false;
        try {
            URL url = new URL(HOST_ADDRESS);
            String response = testWebservice(url);
            if ("OK".equals(response)) {
                available = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            available = false;
        }
        return available;
    }

    private String testWebservice(URL url) throws IOException {
        HttpURLConnection connection = null;
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("HEAD");
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        // connection.setDoInput(true);

        connection.connect();
        String response = connection.getResponseMessage();
        Log.d("Response", response);
        connection.disconnect();
        return response;
    }
}
