package de.andreaswillems.android.mad_todoapp_v3.contacts;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.HashMap;

public class ReadContactData {

    private Context context;

    public ReadContactData(Context context) {
        this.context = context;
    }

    public HashMap<String, String> processContactData(Uri data) {
        HashMap<String, String> contactMap = new HashMap<>();
        contactMap.put("uri", data.toString());
        contactMap = getContactId(data, contactMap);
        contactMap = getContactInfo(contactMap);
        return contactMap;
    }

    private HashMap<String, String> getContactId(Uri uri, HashMap<String, String> contactMap) {
        if (null == uri || null == contactMap) {
            return null;
        }
        Cursor cursor = createSimpleCursor(uri);
        if (null == cursor) {
            return null;
        }
        if (cursor.moveToNext()) {
            String contactIdKey =  ContactsContract.Contacts._ID;
            int contactIdColumnIndex = cursor.getColumnIndex(contactIdKey);
            String contactId = cursor.getString(contactIdColumnIndex);
            contactMap.put(contactIdKey, contactId);
        }
        return contactMap;
    }

    private HashMap<String, String> getContactInfo(HashMap<String, String> contactMap) {
        if (null == contactMap) {
            return null;
        }
        String contactIdKey =  ContactsContract.Contacts._ID;
        String contactNameKey = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;
        String contactPhoneNumberKey = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String contactPhotoURIKey = ContactsContract.Contacts.PHOTO_THUMBNAIL_URI;
        Cursor phoneCursor = createPhoneCursor(contactMap.get(contactIdKey));

        if (phoneCursor.moveToNext()) {
            int contactNumberColumnIndex =
                    phoneCursor.getColumnIndex(contactPhoneNumberKey);
            int contactNameColumnIndex =
                    phoneCursor.getColumnIndex(contactNameKey);
            int contactPhotoColumnIndex =
                    phoneCursor.getColumnIndex(contactPhotoURIKey);
            String contactName = phoneCursor.getString(contactNameColumnIndex);
            String contactNumber = phoneCursor.getString(contactNumberColumnIndex);
            String contactPhotoURI = phoneCursor.getString(contactPhotoColumnIndex);
            contactMap.put(contactNameKey, contactName);
            contactMap.put(contactPhoneNumberKey, contactNumber);
            contactMap.put(contactPhotoURIKey, contactPhotoURI);
        }
        return contactMap;
    }

    private Cursor createSimpleCursor(Uri uri) {
        return context.getContentResolver().query(
                uri, // uri
                null, // projection
                null, // selection
                null, // selectionArgs
                null // sortOrder
        );
    }

    private Cursor createPhoneCursor(String id) {
        return context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, // projection
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + id, // selection
                null, // selectionArgs
                null // sortOrder
        );
    }
}
