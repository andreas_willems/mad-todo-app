package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.list_view;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.R;

/**
 *
 */

public class TodoListAdapter extends RecyclerView.Adapter<TodoListItemViewHolder> {

    private List<TodoItem> todoItemList;

    public TodoListAdapter() {
        this(new ArrayList<TodoItem>());
    }

    public TodoListAdapter(List<TodoItem> todoItemList) {
        this.todoItemList = todoItemList;
    }

    @Override
    public TodoListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new TodoListItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TodoListItemViewHolder holder, int position) {
        holder.setTodoItem(todoItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return todoItemList.size();
    }
}
