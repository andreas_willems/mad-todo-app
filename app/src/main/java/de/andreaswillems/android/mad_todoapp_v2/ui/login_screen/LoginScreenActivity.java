package de.andreaswillems.android.mad_todoapp_v2.ui.login_screen;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.login_screen.LoginScreenPresenter;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactsPermissionGrantedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.LoginErrorEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.LoginSuccessEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.NetworkPermissionGrantedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.WebserviceAvailableEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.WebserviceUnavailableEvent;

public class LoginScreenActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE = 123;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 456;
    private LoginScreenPresenter presenter;
    private ProgressDialog mProgressDialog;
    private TextInputEditText mEmailInput;
    private TextInputEditText mPasswordInput;
    private TextView mMessagesTextView;
    private Button mLoginButton;
    private boolean isValidEmail = false;
    private boolean isValidPassword = false;
    private TextInputLayout mEmailInputLayout;
    private TextInputLayout mPasswordInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        presenter = new LoginScreenPresenter(this);
        mProgressDialog = new ProgressDialog(this);
        checkNetworkPermissions();
        checkContactPermissions();

    }

    private void checkNetworkPermissions() {
        int networkPermissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_NETWORK_STATE
        );
        if (networkPermissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] { Manifest.permission.ACCESS_NETWORK_STATE},
                    MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE
            );
        }
    }

    private void checkContactPermissions() {
        int contactPermissionsCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_CONTACTS
        );
        if (contactPermissionsCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] { Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE: {
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EventBus.getDefault().post(new NetworkPermissionGrantedEvent(
                            "network permission granted"
                    ));
                }
                break;
            }
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EventBus.getDefault().post(new ContactsPermissionGrantedEvent(
                            "network permission granted"
                    ));
                }
                break;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        showProgress("Trying to reach webservice...");
        presenter.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWebserviceAvailable(WebserviceAvailableEvent event) {
        hideProgress();
        initMembers();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWebserviceUnavailable(WebserviceUnavailableEvent event) {
        hideProgress();
        showNetworkErrorAlert();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginSuccessEvent(LoginSuccessEvent event) {
        hideProgress();
        presenter.enterTodoListActivity();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginErrorEvent(LoginErrorEvent event) {
        hideProgress();
        showLoginError(event.getMessage());
    }

    private void initMembers() {
        mEmailInputLayout = (TextInputLayout) findViewById(R.id.login_screen_email_layout);
        mPasswordInputLayout = (TextInputLayout) findViewById(R.id.login_screen_password_layout);
        mEmailInput = (TextInputEditText) findViewById(R.id.login_screen_email_input);
        mPasswordInput = (TextInputEditText) findViewById(R.id.login_screen_password_input);
        mMessagesTextView = (TextView) findViewById(R.id.login_screen_tv_messages);
        mLoginButton = (Button) findViewById(R.id.login_screen_login_button);

        mEmailInput.setOnEditorActionListener(new EmailInputOnEditorActionListener());
        mPasswordInput.setOnEditorActionListener(new PasswordInputOnEditorActionListener());
        mLoginButton.setOnClickListener(new LoginButtonOnClickListener());
    }

    private void showNetworkErrorAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.network_error_message)
                .setTitle(R.string.network_error_title);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.network_error_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    presenter.enterTodoListActivity();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean validateEmail(String email) {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Matcher emailMatcher = emailPattern.matcher(email);
        boolean isValid = emailMatcher.matches();
        if (!isValid) {
            mEmailInputLayout.setErrorEnabled(true);
            mEmailInputLayout.setError(getString(R.string.login_error_invalid_email));
            // mEmailInput.setError(getString(R.string.login_error_invalid_email));
        } else {
            mEmailInputLayout.setErrorEnabled(false);
        }
        isValidEmail = isValid;
        resetMessageTextView();
        checkLoginButtonEnabled();
        return isValid;
    }

    private boolean validatePassword(String password) {
        String passwordRegex = "\\d{6}";
        Pattern passwordPattern = Pattern.compile(passwordRegex);
        Matcher passwordMatcher = passwordPattern.matcher(password);
        boolean isValid = passwordMatcher.matches();
        if (!isValid) {
            mPasswordInputLayout.setErrorEnabled(true);
            mPasswordInputLayout.setError(getString(R.string.login_error_invalid_password));
        } else {
            mPasswordInputLayout.setErrorEnabled(false);
        }
        isValidPassword = isValid;
        resetMessageTextView();
        checkLoginButtonEnabled();
        return isValid;
    }

    private void checkLoginButtonEnabled() {
        if (isValidEmail && isValidPassword) {
            mLoginButton.setEnabled(true);
        } else {
            mLoginButton.setEnabled(false);
        }
    }

    private void showLoginError(String message) {
        mMessagesTextView.setText(message);
        mMessagesTextView.setTextColor(
                getApplicationContext().getColor(R.color.colorDanger)
        );
    }

    private void resetMessageTextView() {
        mMessagesTextView.setText(getString(R.string.login_screen_usage_hint));
        mMessagesTextView.setTextColor(
                getApplicationContext().getColor(R.color.colorBottomSheetBackground)
        );
    }

    private void showProgress(String message) {
        String dialogMessage;
        if (null == message) {
            dialogMessage = getString(R.string.login_request_message);
        } else {
            dialogMessage = message;
        }
        mProgressDialog.setMessage(dialogMessage);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    private void hideProgress() {
        mProgressDialog.hide();
        mProgressDialog.dismiss();
    }

    private class LoginButtonOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String email = mEmailInput.getText().toString();
            String password = mPasswordInput.getText().toString();

            boolean validEmail = validateEmail(email);
            boolean validPassword = validatePassword(password);

            if (validEmail && validPassword) {
                showProgress(null);
                presenter.login(email, password);
            }
        }
    }

    private class EmailInputOnEditorActionListener implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                TextInputEditText emailInput = (TextInputEditText) v;
                String currentInput = emailInput.getText().toString();
                validateEmail(currentInput);
            }
            return false;
        }
    }

    private class PasswordInputOnEditorActionListener implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                TextInputEditText passwordInput = (TextInputEditText) v;
                String currentInput = passwordInput.getText().toString();
                validatePassword(currentInput);
            }

            return false;
        }
    }
}
