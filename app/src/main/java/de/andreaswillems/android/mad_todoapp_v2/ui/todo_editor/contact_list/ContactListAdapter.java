package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.contact_list;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.ContactListPresenter;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListItemViewHolder> {

    private ContactListPresenter presenter;
    private List<ContactItem> contactList;

    ContactListAdapter(ContactListPresenter presenter) {
        this.presenter = presenter;
        this.contactList = new ArrayList<>();
    }

    void addContactToList(ContactItem contact) {
        this.contactList.add(contact);
    }

    public void removeContactFromList(ContactItem contact) {
        this.contactList.remove(contact);
    }

    void clearList() {
        contactList.clear();
    }

    @Override
    public ContactListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_contact, parent, false);
        return new ContactListItemViewHolder(view, presenter);
    }

    @Override
    public void onBindViewHolder(final ContactListItemViewHolder holder, int position) {
        holder.setContactItem(contactList.get(position));
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public void setContactList(List<ContactItem> contacts) {
        this.contactList = contacts;
    }

    public List<ContactItem> getContactList() {
        return contactList;
    }
}
