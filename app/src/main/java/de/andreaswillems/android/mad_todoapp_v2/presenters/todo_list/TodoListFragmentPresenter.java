package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import de.andreaswillems.android.mad_todoapp_v2.presenters.IFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.list_view.TodoListAdapter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.list_view.TodoListFragment;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.ShowListOfTodoItemsUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoListEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoListUpdatedEvent;


public class TodoListFragmentPresenter implements IFragmentPresenter {

    private TodoListFragment mTodoListFragment;
    private ShowListOfTodoItemsUseCase useCase;

    public TodoListFragmentPresenter(TodoListFragment todoListFragment) {
        mTodoListFragment = todoListFragment;
        Context applicationContext =
                mTodoListFragment.getActivity().getApplicationContext();
        useCase = new ShowListOfTodoItemsUseCase(applicationContext);
    }

    public void handleTodoListUpdate(TodoListUpdatedEvent event) {
        mTodoListFragment.setAdapter(new TodoListAdapter(event.getTodoItemList()));
    }

    public void requestTodoList() {
        EventBus.getDefault().post(new RequestTodoListEvent("requesting todo list"));
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onActivityCreated() {

    }

    @Override
    public void onDestroyView() {
    }

    @Override
    public void onDetach() {

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {
        requestTodoList();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onAttach() {

    }
}
