package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners;

import android.app.DialogFragment;
import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments.DatePickerFragment;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;


public class ShowDatePickerOnClickListener implements View.OnClickListener {

    private TodoEditorActivity activity;

    public ShowDatePickerOnClickListener(TodoEditorActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(activity.getFragmentManager(), "datePicker");
    }
}
