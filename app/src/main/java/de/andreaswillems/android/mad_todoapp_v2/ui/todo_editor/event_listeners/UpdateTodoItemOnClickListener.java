package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners;


import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.TodoEditorPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;

public class UpdateTodoItemOnClickListener implements View.OnClickListener {

    private TodoEditorActivity activity;
    private TodoEditorPresenter presenter;

    public UpdateTodoItemOnClickListener(TodoEditorActivity activity,
                                         TodoEditorPresenter presenter) {
        this.activity = activity;
        this.presenter = presenter;
    }

    @Override
    public void onClick(View v) {
        presenter.setItemName(activity.getItemName());
        presenter.setItemDescription(activity.getItemDescription());
        presenter.updateItem();
    }
}