package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners;

import android.view.View;
import android.widget.CheckBox;

import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListBottomSheetFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.fragments.TodoListBottomSheetContentFragment;


public class HideDoneOnClickListener implements View.OnClickListener {

    private TodoListBottomSheetContentFragment fragment;
    private TodoListBottomSheetFragmentPresenter presenter;

    public HideDoneOnClickListener(
            TodoListBottomSheetContentFragment fragment,
            TodoListBottomSheetFragmentPresenter presenter) {
        this.fragment = fragment;
        this.presenter = presenter;
    }

    @Override
    public void onClick(View v) {
        CheckBox checkBox = (CheckBox) v;
        boolean hideDone = checkBox.isChecked();
        if (hideDone) {
            presenter.excludeDoneItems();
        } else {
            presenter.includeDoneItems();
        }
    }
}
