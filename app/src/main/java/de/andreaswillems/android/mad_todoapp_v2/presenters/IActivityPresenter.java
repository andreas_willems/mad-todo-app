package de.andreaswillems.android.mad_todoapp_v2.presenters;

/**
 *
 */

public interface IActivityPresenter {

    public void onCreate();

    public void onStart();

    public void onResume();

    public void onPause();

    public void onStop();

    public void onDestroy();
}
