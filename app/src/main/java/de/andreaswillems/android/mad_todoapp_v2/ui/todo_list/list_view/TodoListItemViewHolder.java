package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.list_view;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListItemPresenter;


public class TodoListItemViewHolder extends RecyclerView.ViewHolder {

    private View mItemView;
    private TodoListItemPresenter presenter;
    private CardView mCardView;
    private TextView mNameTextView;
    private TextView mDueDateTextView;
    private ImageButton mImageButtonDone;
    private ImageButton mImageButtonFavorite;
    private ImageButton mImageButtonEdit;

    TodoListItemViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        presenter = new TodoListItemPresenter(this);

        mCardView = (CardView) itemView.findViewById(R.id.card_view);
        mNameTextView = (TextView) itemView.findViewById(R.id.list_item_tv_name);
        mDueDateTextView = (TextView) itemView.findViewById(R.id.tv_due_date);
        mImageButtonDone = (ImageButton) itemView.findViewById(R.id.button_toggle_done);
        mImageButtonFavorite = (ImageButton) itemView.findViewById(R.id.button_toggle_favorite);
        mImageButtonEdit = (ImageButton) itemView.findViewById(R.id.button_edit);

        mImageButtonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.toggleDoneButton(v);
            }
        });
        mImageButtonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.toggleFavoriteButton(v);
            }
        });

        mImageButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.editButtonClicked(v);
            }
        });
    }

    public void setTodoItem(TodoItem todoItem) {
        presenter.setTodoItem(todoItem);
        mNameTextView.setText(todoItem.getName());
        mDueDateTextView.setText(todoItem.getFormattedDateTime());
        mCardView = setCardBackground(mCardView, todoItem.getDueDate());
        mNameTextView = setTextColor(mNameTextView, todoItem.getDueDate());
        mDueDateTextView = setTextColor(mDueDateTextView, todoItem.getDueDate());

        boolean showIsDoneIcon = todoItem.isDone();
        if (showIsDoneIcon) {
            mImageButtonDone.setImageResource(R.drawable.ic_check_box_checked);
        } else {
            mImageButtonDone.setImageResource(R.drawable.ic_check_box_outline);
        }

        boolean showIsFavoriteIcon = todoItem.isFavorite();
        if (showIsFavoriteIcon) {
            mImageButtonFavorite.setImageResource(R.drawable.ic_favorite_filled);
        } else {
            mImageButtonFavorite.setImageResource(R.drawable.ic_favorite_outline);
        }

    }

    private CardView setCardBackground(CardView cardView, long dueDate) {
        int darkBG = mItemView.getContext().getColor(R.color.colorPrimaryDark);
        int lightBG = mItemView.getContext().getColor(R.color.colorPrimaryLight);

        long now = Calendar.getInstance().getTimeInMillis();
        if (dueDate < now) {
            cardView.setCardBackgroundColor(lightBG);
        } else {
            cardView.setCardBackgroundColor(darkBG);
        }
        return cardView;
    }

    private TextView setTextColor(TextView textView, long dueDate) {
        int darkText = mItemView.getContext().getColor(R.color.colorAccentPrimaryText);
        int lightText = mItemView.getContext().getColor(R.color.colorTextPrimary);
        long now = Calendar.getInstance().getTimeInMillis();
        if (dueDate < now) {
            textView.setTextColor(darkText);
        } else {
            textView.setTextColor(lightText);
        }
        return textView;
    }
}
