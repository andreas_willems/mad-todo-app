package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners;


import android.content.Intent;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.TodoEditorPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;

// import static de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments.TodoEditorBottomSheetContentFragment.MY_PERMISSIONS_REQUEST_READ_CONTACTS;

public class AddContactOnClickListener implements View.OnClickListener {

    private TodoEditorActivity activity;
    private TodoEditorPresenter presenter;

    public AddContactOnClickListener(TodoEditorActivity activity,
                                     TodoEditorPresenter presenter) {
        this.activity = activity;
        this.presenter = presenter;
    }

    @Override
    public void onClick(View v) {
        // checkPermissions();
        Intent pickContact = new Intent(
                Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        this.activity.startActivityForResult(pickContact, TodoEditorActivity.PICK_CONTACT);
    }

    /*private void checkPermissions() {
        boolean denied =
                ContextCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED;
        if (denied) {
            ActivityCompat.requestPermissions(
                    activity,
                    new String[] {android.Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS
            );
        }
    }*/
}