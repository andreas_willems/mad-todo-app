package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments;

import android.app.Fragment;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.TodoEditorPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners.AddContactOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners.DeleteTodoItemOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners.SaveTodoItemOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners.UpdateTodoItemOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchedEvent;


public class TodoEditorBottomSheetContentFragment extends Fragment {

    private Button mSaveButton;
    private Button mUpdateButton;
    private Button mDeleteButton;
    private Button mAddContactButton;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View bottomSheetContentFragmentView =
                inflater.inflate(R.layout.todo_editor_bottomsheet_content,
                        container, false);

        int saveButtonId = R.id.todo_editor_bottom_sheet_save_button;
        int updateButtonId = R.id.todo_editor_bottom_sheet_update_button;
        int deleteButtonId = R.id.todo_editor_bottom_sheet_delete_button;
        int addContactButtonId = R.id.todo_editor_bottom_sheet_add_contact_button;
        mSaveButton = (Button) bottomSheetContentFragmentView
                .findViewById(saveButtonId);
        mUpdateButton = (Button) bottomSheetContentFragmentView
                .findViewById(updateButtonId);
        mDeleteButton = (Button) bottomSheetContentFragmentView
                .findViewById(deleteButtonId);
        mAddContactButton = (Button) bottomSheetContentFragmentView
                .findViewById(addContactButtonId);

        return bottomSheetContentFragmentView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TodoEditorActivity parent = (TodoEditorActivity) getActivity();
        TodoEditorPresenter todoEditorPresenter = parent.getPresenter();

        mSaveButton.setOnClickListener(
                new SaveTodoItemOnClickListener(
                        parent,
                        todoEditorPresenter
                ));
        mUpdateButton.setOnClickListener(
                new UpdateTodoItemOnClickListener(
                        parent,
                        todoEditorPresenter
                ));
        mDeleteButton.setOnClickListener(new DeleteTodoItemOnClickListener(
                parent,
                todoEditorPresenter
        ));
        mAddContactButton.setOnClickListener(new AddContactOnClickListener(
                parent,
                todoEditorPresenter
        ));
        mUpdateButton.setVisibility(View.GONE);
        mDeleteButton.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemFetched(TodoItemFetchedEvent event) {
        TodoItem currentItem = event.getTodoItem();
        if (null != currentItem) {
            if (currentItem.getId() == -1) {
                mSaveButton.setVisibility(View.VISIBLE);
                mUpdateButton.setVisibility(View.GONE);
                mDeleteButton.setVisibility(View.GONE);
            } else {
                mSaveButton.setVisibility(View.GONE);
                mUpdateButton.setVisibility(View.VISIBLE);
                mDeleteButton.setVisibility(View.VISIBLE);
            }
        }
    }

}
