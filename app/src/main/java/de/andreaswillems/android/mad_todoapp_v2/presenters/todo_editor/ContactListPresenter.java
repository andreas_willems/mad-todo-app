package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor;



import android.content.Intent;
import android.net.Uri;
import androidx.fragment.app.FragmentManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.IFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.contact_list.ContactListFragment;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.contact_list.ContactListAdapter;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemUpdateEvent;

public class ContactListPresenter implements IFragmentPresenter {

    private ContactListFragment fragment;
    private ContactListAdapter mAdapter;
    private TodoItem mTodoItem;

    public ContactListPresenter(ContactListFragment fragment) {
        this.fragment = fragment;
    }

    public FragmentManager getFragmentManager() {
        TodoEditorActivity activity = (TodoEditorActivity) fragment.getActivity();
        return activity.getSupportFragmentManager();
    }

    public void setAdapter(ContactListAdapter adapter) {
        mAdapter = adapter;
    }

    public void setTodoItem(TodoItem todoItem) {
        mTodoItem = todoItem;
    }

    public void sendSMS(ContactItem contact) {
        Uri smsURI = Uri.parse("sms:" + contact.getNumber());
        Intent sendSMSIntent = new Intent(Intent.ACTION_VIEW, smsURI);
        String message = mTodoItem.getName()
                + " : "
                + mTodoItem.getDescription();
        sendSMSIntent.putExtra("sms_body", message);
        fragment.startActivity(sendSMSIntent);
    }

    public void removeContact(ContactItem contactItem) {
        List<String> contactItemList = mTodoItem.getContacts();
        boolean removedFromTodoItem = contactItemList.remove(contactItem.getUri());

        List<ContactItem> contactList = mAdapter.getContactList();
        boolean removedFromAdapter = contactList.remove(contactItem);

        if (removedFromTodoItem && removedFromAdapter) {
            EventBus.getDefault().post(new RequestTodoItemUpdateEvent(
                    "requesting update",
                    mTodoItem
            ));
            mAdapter.setContactList(contactList);
            mAdapter.notifyDataSetChanged();
        } else {
            contactItemList.add(contactItem.getUri());
            contactList.add(contactItem);
        }
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onActivityCreated() {

    }

    @Override
    public void onDestroyView() {

    }

    @Override
    public void onDetach() {

    }
}
