package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListBottomSheetFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners.DeleteTodoItemsOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners.HideDoneOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners.OnlyFavouriteOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners.SortModeOnCheckedChangeListener;


public class TodoListBottomSheetContentFragment extends Fragment {

    private TodoListBottomSheetFragmentPresenter presenter;
    private Button mDeleteAllButton;
    private CheckBox mHideDoneCheckBox;
    private CheckBox mOnlyFavouriteCheckBox;
    private RadioGroup mSortModeRadioGroup;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(
                R.layout.todo_list_bottomsheet_content,
                container,
                false
        );

        presenter = new TodoListBottomSheetFragmentPresenter(this);

        mDeleteAllButton = (Button) view
                .findViewById(R.id.todo_list_bottom_sheet_delete_button);
        mHideDoneCheckBox = (CheckBox) view
                .findViewById(R.id.cb_bottom_sheet_filter_done_items);
        mOnlyFavouriteCheckBox = (CheckBox) view
                .findViewById(R.id.cb_bottom_sheet_filter_favorite_items);
        mSortModeRadioGroup = (RadioGroup) view
                .findViewById(R.id.todo_list_bottom_sheet_sort_radio_group);
        mDeleteAllButton.setOnClickListener(
                new DeleteTodoItemsOnClickListener(this, presenter)
        );
        mHideDoneCheckBox.setOnClickListener(
                new HideDoneOnClickListener(this, presenter)
        );
        mOnlyFavouriteCheckBox.setOnClickListener(
                new OnlyFavouriteOnClickListener(this, presenter)
        );
        mSortModeRadioGroup.setOnCheckedChangeListener(
                new SortModeOnCheckedChangeListener(presenter)
        );

        return view;
    }
}
