package de.andreaswillems.android.mad_todoapp_v2.presenters.login_screen;

import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

import de.andreaswillems.android.mad_todoapp_v2.presenters.IActivityPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.login_screen.LoginScreenActivity;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.TodoListActivity;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.LoginAtWebserviceUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestLoginEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.WebserviceAvailableRequestEvent;

/**
 *
 */

public class LoginScreenPresenter implements IActivityPresenter {

    private LoginScreenActivity mLoginScreenActivity;

    public LoginScreenPresenter(LoginScreenActivity loginScreenActivity) {
        mLoginScreenActivity = loginScreenActivity;
        LoginAtWebserviceUseCase useCase = LoginAtWebserviceUseCase
                .getInstance(loginScreenActivity.getApplicationContext());
    }


    public void login(String email, String password) {
        EventBus.getDefault().post(new RequestLoginEvent("requesting login", email, password));
    }

    public void enterTodoListActivity() {
        Intent changeActivityIntent = new Intent(mLoginScreenActivity, TodoListActivity.class);
        mLoginScreenActivity.finish();
        mLoginScreenActivity.startActivity(changeActivityIntent);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {
        EventBus.getDefault().post(new WebserviceAvailableRequestEvent(
                "requesting webservice available"
        ));
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

}
