package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners;


import androidx.annotation.IdRes;
import android.widget.RadioGroup;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListBottomSheetFragmentPresenter;

public class SortModeOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {

    private TodoListBottomSheetFragmentPresenter presenter;

    public SortModeOnCheckedChangeListener(
            TodoListBottomSheetFragmentPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.radio_due_date: {
                presenter.sortByDueDate();
                break;
            }
            case R.id.radio_favorite: {
                presenter.sortByFavorite();
                break;
            }
            default: {
                presenter.sortByDueDate();
            }
        }
    }
}
