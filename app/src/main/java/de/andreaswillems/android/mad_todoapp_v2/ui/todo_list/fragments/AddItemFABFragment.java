package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.fragments;

import android.app.Fragment;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.AddItemFABPresenter;

/**
 *
 */

public class AddItemFABFragment extends Fragment {

    private AddItemFABPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View fabView = inflater.inflate(R.layout.add_item_fab_fragment, container, false);

        presenter = new AddItemFABPresenter(fabView);
        presenter.onCreateView();

        return fabView;
    }

    public AddItemFABPresenter getPresenter() {
        return presenter;
    }

    public FloatingActionButton getAddItemFAB() {
        return presenter.getAddItemFAB();
    }
}
