package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.contact_list;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.ContactListPresenter;

class ContactListItemViewHolder extends ViewHolder
        implements View.OnLongClickListener {

    private TextView idTextView;
    private TextView contentTextView;
    private ContactListPresenter mPresenter;
    private ContactItem mContact;

    ContactListItemViewHolder(View itemView, ContactListPresenter presenter) {
        super(itemView);
        mPresenter = presenter;
        contentTextView = (TextView) itemView.findViewById(R.id.contact_list_item_tv_content);
        itemView.setOnLongClickListener(this);
    }

    void setContactItem(ContactItem contact) {
        mContact = contact;
        contentTextView.setText(contact.getName());
    }

    @Override
    public boolean onLongClick(View v) {
        ContactActionsDialogFragment fragment = new ContactActionsDialogFragment();
        fragment.setPresenter(mPresenter);
        fragment.setContactItem(mContact);
        fragment.show(mPresenter.getFragmentManager(), fragment.getTag());
        return false;
    }
}
