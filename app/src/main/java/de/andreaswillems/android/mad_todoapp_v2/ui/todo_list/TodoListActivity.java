package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.fragments.AddItemFABFragment;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdateFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdatedEvent;

public class TodoListActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private TodoListPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        presenter = new TodoListPresenter(this);
        presenter.onCreate();

        AddItemFABFragment fabFragment = (AddItemFABFragment) getFragmentManager()
                .findFragmentById(R.id.todo_list_add_item_fragment);

        fab = fabFragment.getPresenter().getAddItemFAB();
        View bottomSheet = findViewById(R.id.todo_list_bottom_sheet);

        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setBottomSheetCallback(new ScaleFabCallback());
    }


    private class ScaleFabCallback extends BottomSheetBehavior.BottomSheetCallback {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (BottomSheetBehavior.STATE_DRAGGING == newState) {
                fab.animate().scaleX(0).scaleY(0).setDuration(300).start();
            } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                fab.animate().scaleX(1).scaleY(1).setDuration(300).start();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemUpdate(TodoItemUpdatedEvent event) {
        showMessage(event.getMessage());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemUpdateFailed(TodoItemUpdateFailedEvent event) {
        showMessage(event.getMessage());
    }


    private void showMessage(String message) {
        CoordinatorLayout coordinatorLayout =
                (CoordinatorLayout) findViewById(R.id.todo_list_coordinator_layout);
        if (message.length() > 0) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        presenter.onStart();
    }


    @Override
    public void onStop() {
        presenter.onStop();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
