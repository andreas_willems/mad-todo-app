package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.IActivityPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.CreateTodoItemUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.DeleteTodoItemUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.ReadContactDataUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.ReadTodoItemUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.UpdateTodoItemUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.CreateTodoItemEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestContactDataEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestContactsForTodoEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemDeleteEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemUpdateEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchedEvent;


public class TodoEditorPresenter implements IActivityPresenter {

    private TodoEditorActivity mTodoEditorActivity;
    private Context applicationContext;
    private CreateTodoItemUseCase createTodoItemUseCase;
    private ReadTodoItemUseCase readTodoItemUseCase;
    private UpdateTodoItemUseCase updateTodoItemUseCase;
    private DeleteTodoItemUseCase deleteTodoItemUseCase;
    private ReadContactDataUseCase readContactDataUseCase;
    private TodoItem mTodoItem;

    public TodoEditorPresenter(TodoEditorActivity todoEditorActivity) {
        mTodoEditorActivity = todoEditorActivity;
        applicationContext = mTodoEditorActivity.getApplicationContext();
    }

    public void saveItem() {
        EventBus.getDefault().post(new CreateTodoItemEvent("create event", mTodoItem));
    }

    public void setItem(TodoItem todoItem) {
        mTodoItem = todoItem;
        requestContactData(mTodoItem);
    }

    public TodoItem getItem() {
        if (null == mTodoItem) {
            mTodoItem = new TodoItem();
        }
        return mTodoItem;
    }

    public void deleteItem() {
        EventBus.getDefault().post(new RequestTodoItemDeleteEvent(
                "requesting delete", mTodoItem
        ));
    }

    public void updateItem() {
        EventBus.getDefault().post(new RequestTodoItemUpdateEvent(
                "requesting update", mTodoItem
        ));
    }

    public void processContactData(Uri data) {
        EventBus.getDefault().post(new RequestContactDataEvent(
                "requesting contact data", data
        ));
    }

    public void setItemName(String name) {
        mTodoItem.setName(name);
    }

    public void setItemDescription(String description) {
        mTodoItem.setDescription(description);
    }

    public void setDate(int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.mTodoItem.getDueDate());
        calendar.set(year, month, dayOfMonth);
        mTodoItem.setDueDate(calendar.getTimeInMillis());
    }

    public void setTime(int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.mTodoItem.getDueDate());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        mTodoItem.setDueDate(calendar.getTimeInMillis());
    }
    public void toggleDoneButton() {
        mTodoItem.setDone(!mTodoItem.isDone());
        if (mTodoItem.getId() > -1) {
            requestUpdate(mTodoItem);
        } else {
            mTodoEditorActivity.updateDoneButtonIcon(mTodoItem.isDone());
        }
    }

    public void toggleFavoriteButton() {
        mTodoItem.setFavorite(!mTodoItem.isFavorite());
        if (mTodoItem.getId() > -1) {
            requestUpdate(mTodoItem);
        } else {
            mTodoEditorActivity.updateFavoriteButtonIcon(mTodoItem.isFavorite());
        }
    }

    public void handleFetchedContact(ContactItem contact) {
        mTodoItem.addContact(contact.getUri());
    }

    private void requestUpdate(TodoItem todoItem) {
        EventBus.getDefault().post(new RequestTodoItemUpdateEvent(
                "requesting todo item update",
                todoItem
        ));
    }

    private void requestContactData(TodoItem todoItem) {
        EventBus.getDefault().post(
                new RequestContactsForTodoEvent("requesting contacts", todoItem)
        );
    }

    @Override
    public void onCreate() {}

    @Override
    public void onStart() {
        createTodoItemUseCase =
                CreateTodoItemUseCase.getInstance(applicationContext);
        readTodoItemUseCase =
                ReadTodoItemUseCase.getInstance(applicationContext);
        updateTodoItemUseCase =
                UpdateTodoItemUseCase.getInstance(applicationContext);
        deleteTodoItemUseCase =
                DeleteTodoItemUseCase.getInstance(applicationContext);
        readContactDataUseCase =
                ReadContactDataUseCase.getInstance(applicationContext);
    }

    @Override
    public void onResume() {
        Intent incomingIntent = mTodoEditorActivity.getIntent();
        if (incomingIntent.hasExtra(Intent.EXTRA_TEXT)) {
            long fallbackId = -1;
            long id = incomingIntent.getLongExtra(Intent.EXTRA_TEXT, fallbackId);
            mTodoEditorActivity.showProgress();
            EventBus.getDefault().post(new RequestTodoItemEvent("requesting item", id));
        } else {
            mTodoItem = new TodoItem("");
            mTodoEditorActivity.onTodoItemFetched(
                    new TodoItemFetchedEvent("Item fetched", mTodoItem)
            );
        }
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onStop() {}

    @Override
    public void onDestroy() {}
}
