package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.IFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;


public class AddItemFABPresenter implements IFragmentPresenter {

    private View mAddItemFABView;
    private FloatingActionButton mAddItemFAB;

    public AddItemFABPresenter(View fabView) {
        mAddItemFABView = fabView;
    }

    public FloatingActionButton getAddItemFAB() {
        return mAddItemFAB;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onCreateView() {
        mAddItemFAB = (FloatingActionButton) mAddItemFABView.findViewById(R.id.fab_add_item);
        mAddItemFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = mAddItemFABView.getContext();
                Intent changeActivity = new Intent(context, TodoEditorActivity.class);
                context.startActivity(changeActivity);
            }
        });
    }

    @Override
    public void onActivityCreated() {

    }

    @Override
    public void onDestroyView() {

    }

    @Override
    public void onDetach() {

    }
}
