package de.andreaswillems.android.mad_todoapp_v2.presenters;

/**
 *
 */

public interface IFragmentPresenter extends IActivityPresenter {

    public void onAttach();

    public void onCreateView();

    public void onActivityCreated();

    public void onDestroyView();

    public void onDetach();
}
