package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.event_listeners;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListBottomSheetFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.fragments.TodoListBottomSheetContentFragment;


public class DeleteTodoItemsOnClickListener implements View.OnClickListener {

    private TodoListBottomSheetContentFragment fragment;
    private TodoListBottomSheetFragmentPresenter presenter;

    public DeleteTodoItemsOnClickListener(
            TodoListBottomSheetContentFragment fragment,
            TodoListBottomSheetFragmentPresenter presenter) {
        this.fragment = fragment;
        this.presenter = presenter;
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());
        builder.setMessage(R.string.delete_all_todos_message)
                .setTitle(R.string.delete_all_todos_title);
        builder.setPositiveButton(R.string.positive_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    presenter.deleteAllItems();
                }
            }
        });
        builder.setNegativeButton(R.string.negative_button_text,
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
