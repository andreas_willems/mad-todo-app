package de.andreaswillems.android.mad_todoapp_v2.ui.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import de.andreaswillems.android.mad_todoapp_v2.ui.login_screen.LoginScreenActivity;

/**
 *
 */

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, LoginScreenActivity.class);
        startActivity(intent);
        finish();
    }
}
