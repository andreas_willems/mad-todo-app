package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.contact_list;


import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.ContactListPresenter;


public class ContactActionsDialogFragment extends BottomSheetDialogFragment {

    private ContactItem mContact;
    private ContactListPresenter mPresenter;
    private TextView mTitle;
    private View mSendSms;
    // private View mSendEmail;
    private View mRemoveContact;

    public void setContactItem(ContactItem contact) {
        mContact = contact;
    }

    public void setPresenter(ContactListPresenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.contact_actions_sheet_fragment, container, false);

        mTitle = (TextView) view.findViewById(R.id.contact_actions_title);
        mTitle.setText(mContact.getName());

        mSendSms = view.findViewById(R.id.contact_action_send_sms);
        mSendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMS();
            }
        });

        /*
        mSendEmail = view.findViewById(R.id.contact_action_send_email);
        mSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });
        */

        mRemoveContact = view.findViewById(R.id.contact_action_remove_contact);
        mRemoveContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeContact();
            }
        });

        return view;
    }

    private void sendSMS() {
        mPresenter.sendSMS(mContact);
    }

    /*
    private void sendEmail() {
        String[] content = {
                "Hello World"
        };
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.putExtra(Intent.EXTRA_TEXT, "HEllo World");
        // sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, content);
        sendEmailIntent.setType("text/plain");
        Intent chooser = Intent.createChooser(sendEmailIntent, "Nachricht senden");
        startActivity(chooser);

    }
    */

    private void removeContact() {
        mPresenter.removeContact(mContact);
    }
}
