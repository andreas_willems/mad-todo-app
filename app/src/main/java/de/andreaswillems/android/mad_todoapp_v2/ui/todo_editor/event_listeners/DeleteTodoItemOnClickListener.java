package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.TodoEditorPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;

public class DeleteTodoItemOnClickListener implements View.OnClickListener {

    private TodoEditorActivity activity;
    private TodoEditorPresenter presenter;

    public DeleteTodoItemOnClickListener(TodoEditorActivity activity,
                                         TodoEditorPresenter presenter) {
        this.activity = activity;
        this.presenter = presenter;
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.delete_dialog_message)
                .setTitle(R.string.delete_dialog_title);
        builder.setPositiveButton(R.string.positive_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    presenter.deleteItem();
                }
            }
        });
        builder.setNegativeButton(R.string.negative_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
