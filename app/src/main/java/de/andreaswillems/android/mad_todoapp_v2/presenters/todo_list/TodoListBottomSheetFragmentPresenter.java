package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import de.andreaswillems.android.mad_todoapp_v2.presenters.IFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.fragments.TodoListBottomSheetContentFragment;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.DeleteAllItemsRequestEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.HideDoneItemsEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.OnlyFavouriteItemsEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.SortByDueDateEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.SortByFavoriteEvent;


public class TodoListBottomSheetFragmentPresenter implements IFragmentPresenter {

    private TodoListBottomSheetContentFragment fragment;

    public TodoListBottomSheetFragmentPresenter(
            TodoListBottomSheetContentFragment fragment) {
        this.fragment = fragment;
    }

    public void deleteAllItems() {
        EventBus.getDefault().post(
                new DeleteAllItemsRequestEvent("requesting all items deleted")
        );
    }

    public void includeDoneItems() {
        EventBus.getDefault().post(
                new HideDoneItemsEvent(
                        "Include done items",
                        HideDoneItemsEvent.INCLUDE
                )
        );
    }

    public void excludeDoneItems() {
        EventBus.getDefault().post(
                new HideDoneItemsEvent(
                        "Exclude done items",
                        HideDoneItemsEvent.EXCLUDE
                )
        );
    }

    public void includeNonFavouriteItems() {
        EventBus.getDefault().post(
                new OnlyFavouriteItemsEvent(
                        "Exclude non favourite items",
                        OnlyFavouriteItemsEvent.EXCLUDE
                )
        );
    }

    public void excludeNonFavouriteItems() {
        EventBus.getDefault().post(
                new OnlyFavouriteItemsEvent(
                        "Include non favourite items",
                        OnlyFavouriteItemsEvent.INCLUDE
                )
        );
    }

    public void sortByDueDate() {
        EventBus.getDefault().post(
                new SortByDueDateEvent("sort by due date")
        );

    }

    public void sortByFavorite() {
        EventBus.getDefault().post(
                new SortByFavoriteEvent("sort by favorite")
        );
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onActivityCreated() {

    }

    @Override
    public void onDestroyView() {

    }

    @Override
    public void onDetach() {

    }
}
