package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.list_view.TodoListItemViewHolder;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemUpdateEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdatedEvent;


public class TodoListItemPresenter {

    private TodoListItemViewHolder mViewHolder;
    private TodoItem mTodoItem;

    public TodoListItemPresenter(TodoListItemViewHolder viewHolder) {
        mViewHolder = viewHolder;
        EventBus.getDefault().register(this);
    }

    public void setTodoItem(TodoItem todoItem) {
        mTodoItem = todoItem;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemUpdated(TodoItemUpdatedEvent event) {
        if (event.getTodoItem().getId() == mTodoItem.getId()) {
            mTodoItem = event.getTodoItem();
            mViewHolder.setTodoItem(mTodoItem);
        }
    }

    public void toggleDoneButton(View v) {
        mTodoItem.setDone(!mTodoItem.isDone());
        requestUpdate(mTodoItem);
    }

    public void toggleFavoriteButton(View v) {
        mTodoItem.setFavorite(!mTodoItem.isFavorite());
        requestUpdate(mTodoItem);
    }

    public void editButtonClicked(View v) {
        final Context context = mViewHolder.itemView.getContext();
        Intent changeActivity = new Intent(context, TodoEditorActivity.class);
        changeActivity.putExtra(Intent.EXTRA_TEXT, mTodoItem.getId());
        context.startActivity(changeActivity);
    }

    private void requestUpdate(TodoItem todoItem) {
        EventBus.getDefault().post(new RequestTodoItemUpdateEvent(
                "requesting todo item update",
                todoItem
        ));
    }
}
