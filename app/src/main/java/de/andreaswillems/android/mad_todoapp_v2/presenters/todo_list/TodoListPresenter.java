package de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list;

import android.app.Activity;

import de.andreaswillems.android.mad_todoapp_v2.presenters.IActivityPresenter;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.DeleteAllTodoItemsUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.SortFilterListOfTodoItemsUseCase;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.UpdateTodoItemUseCase;


public class TodoListPresenter implements IActivityPresenter {

    private Activity mTodoListActivity;

    public TodoListPresenter(Activity todoListActivity) {
        mTodoListActivity = todoListActivity;
    }

    @Override
    public void onCreate() {
        UpdateTodoItemUseCase.getInstance(
                mTodoListActivity.getApplicationContext()
        );

        DeleteAllTodoItemsUseCase.getInstance(
                mTodoListActivity.getApplicationContext()
        );

        SortFilterListOfTodoItemsUseCase.getInstance();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
