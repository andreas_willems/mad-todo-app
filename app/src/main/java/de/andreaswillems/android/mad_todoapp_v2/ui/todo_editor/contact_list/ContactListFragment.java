package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.contact_list;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.ContactListPresenter;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactFetchedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactListFetchedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchedEvent;

public class ContactListFragment extends Fragment {

    private ContactListPresenter presenter;
    private RecyclerView recyclerView;
    private ContactListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(
                R.layout.fragment_contact_list,
                container,
                false
        );
        presenter = new ContactListPresenter(this);


        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            adapter = new ContactListAdapter(presenter);
            recyclerView.setAdapter(adapter);
            presenter.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        presenter.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStop();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactItemFetched(ContactFetchedEvent event) {
        this.adapter.addContactToList(event.getContact());
        this.adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactListFetched(ContactListFetchedEvent event) {
        this.adapter.setContactList(event.getContacts());
        this.adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemFetched(TodoItemFetchedEvent event) {
        presenter.setTodoItem(event.getTodoItem());
    }

}
