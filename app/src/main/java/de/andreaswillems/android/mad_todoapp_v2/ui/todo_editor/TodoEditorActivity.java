package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_editor.TodoEditorPresenter;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners.ShowDatePickerOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners.ShowTimePickerOnClickListener;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments.DatePickedEvent;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments.TimePickedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactFetchFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactFetchedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemCreatedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemCreationFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemDeleteFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemDeletedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdateFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdatedEvent;


public class TodoEditorActivity extends AppCompatActivity {

    final public static int PICK_CONTACT = 1;

    private TodoEditorPresenter presenter;
    private TextInputEditText mEditTextName;
    private TextInputEditText mEditTextDescription;
    private TextView mTextViewDate;
    private TextView mTextViewTime;
    private ProgressDialog mProgressDialog;
    private ImageButton mShowDatePickerButton;
    private ImageButton mShowTimePickerButton;
    private ImageButton mDoneButton;
    private ImageButton mFavoriteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_editor);
        initMembers();

        presenter = new TodoEditorPresenter(this);
        presenter.onCreate();
    }

    private void initMembers() {
        mProgressDialog = new ProgressDialog(this);
        mEditTextName = (TextInputEditText) findViewById(R.id.et_todo_item_name_input);
        mEditTextDescription = (TextInputEditText) findViewById(R.id.et_todo_item_description_input);
        mTextViewDate = (TextView) findViewById(R.id.todo_editor_date_output);
        mTextViewTime = (TextView) findViewById(R.id.todo_editor_time_output);
        mShowDatePickerButton =
                (ImageButton) findViewById(R.id.todo_editor_calendar_button);
        mShowTimePickerButton =
                (ImageButton) findViewById(R.id.todo_editor_time_button);
        mDoneButton =
                (ImageButton) findViewById(R.id.todo_editor_is_done_button);
        mFavoriteButton =
                (ImageButton) findViewById(R.id.todo_editor_is_favorite_button);

        mShowDatePickerButton.setOnClickListener(
                new ShowDatePickerOnClickListener(this));
        mShowTimePickerButton.setOnClickListener(
                new ShowTimePickerOnClickListener(this));
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.toggleDoneButton();
            }
        });
        mFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.toggleFavoriteButton();
            }
        });
    }

    private void updateView(TodoItem todoItem) {
        mEditTextName.setText(todoItem.getName());
        mEditTextDescription.setText(todoItem.getDescription());
        mTextViewDate.setText(todoItem.getFormattedDate());
        mTextViewTime.setText(todoItem.getFormattedTime());
        updateDoneButtonIcon(todoItem.isDone());
        updateFavoriteButtonIcon(todoItem.isFavorite());
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            presenter.processContactData(data.getData());
        }
    }

    public TodoEditorPresenter getPresenter() { return presenter; }

    public String getItemName() {
        return this.mEditTextName.getText().toString();
    }

    public String getItemDescription() {
        return this.mEditTextDescription.getText().toString();
    }

    public void showProgress() {
        mProgressDialog.setMessage("Fetching Todo...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    public void hideProgress() {
        mProgressDialog.hide();
        mProgressDialog.dismiss();
    }

    public void updateDoneButtonIcon(boolean isDone) {
        if (isDone) {
            mDoneButton.setImageResource(R.drawable.ic_check_box_checked);
        } else {
            mDoneButton.setImageResource(R.drawable.ic_check_box_outline);
        }
    }

    public void updateFavoriteButtonIcon(boolean isFavorite) {
        if (isFavorite) {
            mFavoriteButton.setImageResource(R.drawable.ic_favorite_filled);
        } else {
            mFavoriteButton.setImageResource(R.drawable.ic_favorite_outline);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemFetched(TodoItemFetchedEvent event) {
        hideProgress();
        updateView(event.getTodoItem());
        presenter.setItem(event.getTodoItem());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemFetchFailed(TodoItemFetchFailedEvent event) {
        hideProgress();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDatePicked(DatePickedEvent event) {
        presenter.setDate(
                event.getYear(),
                event.getMonth(),
                event.getDayOfMonth()
        );
        mTextViewDate.setText(presenter.getItem().getFormattedDate());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTimePicked(TimePickedEvent event) {
        presenter.setTime(
                event.getHourOfDay(),
                event.getMinute()
        );
        mTextViewTime.setText(presenter.getItem().getFormattedTime());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemCreated(TodoItemCreatedEvent event) {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemCreationFailed(TodoItemCreationFailedEvent event) {
        showMessage(event.getMessage());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemUpdated(TodoItemUpdatedEvent event) {
        updateView(event.getTodoItem());
        showMessage(event.getMessage());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemUpdateFailed(TodoItemUpdateFailedEvent event) {
        showMessage(event.getMessage());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemDeleted(TodoItemDeletedEvent event) {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoItemDeleteFailed(TodoItemDeleteFailedEvent event) {
        showMessage(event.getMessage());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactDataFetched(ContactFetchedEvent event) {
        showMessage(event.getMessage());
        presenter.handleFetchedContact(event.getContact());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactFetchFailedEvent(ContactFetchFailedEvent event) {
        showMessage(event.getMessage());
    }

    private void showMessage(String message) {
        CoordinatorLayout coordinatorLayout =
                (CoordinatorLayout) findViewById(R.id.todo_editor_coordinator_layout);
        if (message.length() > 0) {
            Snackbar snackbar = Snackbar.make(
                    coordinatorLayout,
                    message,
                    Snackbar.LENGTH_LONG
            );
            snackbar.show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        presenter.onStop();
        super.onStop();
    }
}
