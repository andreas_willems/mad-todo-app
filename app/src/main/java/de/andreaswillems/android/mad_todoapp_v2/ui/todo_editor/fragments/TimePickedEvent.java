package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments;


public class TimePickedEvent {

    private int hourOfDay;
    private int minute;

    public TimePickedEvent(int hourOfDay, int minute) {
        this.hourOfDay = hourOfDay;
        this.minute = minute;
    }

    public int getHourOfDay() {
        return hourOfDay;
    }

    public int getMinute() {
        return minute;
    }
}
