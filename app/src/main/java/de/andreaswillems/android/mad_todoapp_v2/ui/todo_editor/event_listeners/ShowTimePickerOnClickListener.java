package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.event_listeners;

import android.app.DialogFragment;
import android.view.View;

import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments.TimePickerFragment;
import de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.TodoEditorActivity;


public class ShowTimePickerOnClickListener implements View.OnClickListener {

    private TodoEditorActivity activity;

    public ShowTimePickerOnClickListener(TodoEditorActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(activity.getFragmentManager(), "timePicker");
    }
}
