package de.andreaswillems.android.mad_todoapp_v2.ui.todo_editor.fragments;


public class DatePickedEvent {

    private int year;
    private int month;
    private int dayOfMonth;

    public DatePickedEvent(int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }
}
