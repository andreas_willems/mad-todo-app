package de.andreaswillems.android.mad_todoapp_v2.ui.todo_list.list_view;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.R;
import de.andreaswillems.android.mad_todoapp_v2.presenters.todo_list.TodoListFragmentPresenter;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoListUpdatedEvent;


public class TodoListFragment extends Fragment {

    private TodoListFragmentPresenter presenter;
    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public void setAdapter(TodoListAdapter adapter) {
        mAdapter = adapter;
        mRecyclerView.setAdapter(adapter);
    }

    private void showProgress() {
        mProgressDialog.setMessage("Fetching Todos...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    private void hideProgress() {
        mProgressDialog.hide();
        mProgressDialog.dismiss();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View listView = inflater.inflate(R.layout.list_fragment, container, false);

        presenter = new TodoListFragmentPresenter(this);

        mProgressDialog = new ProgressDialog(getContext());
        mRecyclerView = (RecyclerView) listView
                .findViewById(R.id.todo_list_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new TodoListAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout =
                (SwipeRefreshLayout) listView.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.requestTodoList();
            }
        });

        return listView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        showProgress();
        presenter.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStop();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodoListUpdatedEvent(TodoListUpdatedEvent event) {
        hideProgress();
        presenter.handleTodoListUpdate(event);
    }

}
