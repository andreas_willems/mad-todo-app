package de.andreaswillems.android.mad_todoapp_v2.repositories;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.network.DataItemsRestApiOperationsImpl;
import de.andreaswillems.android.mad_todoapp_v2.storage.CRUDOperationsImpl;
import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.DataItem;
import de.andreaswillems.android.mad_todoapp_v2.storage.ICRUDOperations;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.LoginAtWebserviceUseCase;
import de.andreaswillems.android.mad_todoapp_v3.connection.NetworkConnection;


public class TodoListRepositoryImpl implements TodoListRepository {

    private ICRUDOperations crudOperations;
    private DataItemsRestApiOperationsImpl restOperations;

    public TodoListRepositoryImpl(Context context) {
        crudOperations = new CRUDOperationsImpl(context);
        restOperations = new DataItemsRestApiOperationsImpl();
    }

    @Override
    public long saveItem(TodoItem todoItem) {
        return crudOperations.create(
                todoItem.getName(),
                todoItem.getDescription(),
                todoItem.getDueDate(),
                todoItem.isDone(),
                todoItem.isFavorite(),
                asCSV(todoItem.getContacts()),
                ""
        );
    }

    public boolean saveRemoteItem(TodoItem todoItem) {
        if (LoginAtWebserviceUseCase.isWebserviceAvailable()) {
            DataItem dataItem = convertTodoItem(todoItem);
            DataItem syncedItem = restOperations.createDataItem(dataItem);
            return null != syncedItem;
        }
        return false;

    }

    @Override
    public void saveListOfItems(List<TodoItem> listOfItems) {
        List<TodoItem> todoItems = this.getItems();
        for (TodoItem item : todoItems) {
            boolean result = this.saveRemoteItem(item);
            if (!result) {
                throw new RuntimeException();
            }
        }
    }

    @Override
    public List<TodoItem> getItems() {
        List<DataItem> dataItems = crudOperations.readAll();
        return convertDataItems(dataItems);
    }

    @Override
    public List<TodoItem> getItemsRemote() {
        if (LoginAtWebserviceUseCase.isWebserviceAvailable()) {
            List<DataItem> dataItemsList = restOperations.readAllDataItems();
            return convertDataItems(dataItemsList);
        }
        return null;

    }

    @Override
    public TodoItem getItem(long id) {
        DataItem dataItem = crudOperations.read(id);
        if (null != dataItem) {
            return convertDataItem(dataItem);
        } else {
            return null;
        }
    }

    @Override
    public boolean deleteItem(long id) {
        boolean success = crudOperations.delete(id);
        if (success) {
            deleteRemoteItem(id);
        } else {
            throw new RuntimeException(
                    "Deleting local item failed. ID:" + id
            );
        }
        return success;
    }

    @Override
    public boolean deleteRemoteItem(long id) {
        boolean success = false;
        if (LoginAtWebserviceUseCase.isWebserviceAvailable()) {
            success = restOperations.deleteDataItem(id);
            if (!success) {
                throw new RuntimeException(
                        "Deleting remote item failed. ID:" + id
                );
            }
        }
        return success;
    }

    @Override
    public boolean deleteRemoteItems() {
        return restOperations.deleteAllItems();
    }

    @Override
    public boolean deleteLocalItems() {
        return crudOperations.deleteAll();
    }

    @Override
    public boolean updateItem(TodoItem updatedItem) {
        return crudOperations.update(
                updatedItem.getId(),
                updatedItem.getName(),
                updatedItem.getDescription(),
                updatedItem.getDueDate(),
                updatedItem.isDone(),
                updatedItem.isFavorite(),
                asCSV(updatedItem.getContacts()),
                ""
        );
    }

    @Override
    public boolean updateRemoteItem(TodoItem todoItem) {
        if (LoginAtWebserviceUseCase.isWebserviceAvailable()) {
            DataItem dataItem = convertTodoItem(todoItem);
            return restOperations.updateDataItem(dataItem.getId(), dataItem);
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return crudOperations.itemCount();
    }

    private List<TodoItem> convertDataItems(List<DataItem> dataItems) {
        List<TodoItem> todoItems = new ArrayList<>();
        for (DataItem dataItem : dataItems) {
            todoItems.add(convertDataItem(dataItem));
        }
        return todoItems;
    }

    private TodoItem convertDataItem(DataItem dataItem) {
        TodoItem todoItem = new TodoItem(
                dataItem.getName(),
                dataItem.getDescription(),
                dataItem.isDone(),
                dataItem.isFavourite(),
                dataItem.getExpiry()
        );
        todoItem.setId(dataItem.getId());
        todoItem.setContacts(dataItem.getContacts());
        // todoItem.setLocation(dataItem.getLocation());
        return todoItem;
    }

    private DataItem convertTodoItem(TodoItem todoItem) {
        return new DataItem(
                todoItem.getId(),
                todoItem.getName(),
                todoItem.getDescription(),
                todoItem.getDueDate(),
                todoItem.isDone(),
                todoItem.isFavorite(),
                todoItem.getContacts(),
                null
        );
    }

    private String asCSV(List<String> listOfStrings) {
        StringBuilder sb = new StringBuilder();
        for (String str : listOfStrings) {
            sb.append(str);
            sb.append(";");
        }
        return sb.toString();
    }
}
