package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class ContactsPermissionGrantedEvent extends Event {

    public ContactsPermissionGrantedEvent(String message) {
        super(message);
    }

}
