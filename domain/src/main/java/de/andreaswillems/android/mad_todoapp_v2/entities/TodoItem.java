package de.andreaswillems.android.mad_todoapp_v2.entities;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class TodoItem {
    private long id;
    private String name;
    private String description;
    private boolean isDone;
    private boolean isFavorite;
    private long dueDate;
    private List<String> contacts;

    public TodoItem(String name) {
        this(name, "", false, false, Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Copy constructor.
     * @param todoItem - the object to clone
     */
    public TodoItem(TodoItem todoItem) {
        this.id = todoItem.id;
        this.name = todoItem.name;
        this.description = todoItem.description;
        this.isDone = todoItem.isDone;
        this.isFavorite = todoItem.isFavorite;
        this.dueDate = todoItem.dueDate;
        this.contacts = todoItem.getContacts();
    }

    /**
     * Creates an instance of a TodoItem with the given parameters.
     * @param name
     * @param description
     * @param isDone
     * @param isFavorite
     * @param dueDate
     */
    public TodoItem(String name,
                    String description,
                    boolean isDone,
                    boolean isFavorite,
                    long dueDate) {
        this.id = -1; // means id is not set by storage in instantiation
        this.name = name;
        this.description = description;
        this.isDone = isDone;
        this.isFavorite = isFavorite;
        this.dueDate = dueDate;
        this.contacts = new ArrayList<>();
    }

    public TodoItem(String name,
                    String description,
                    boolean isDone,
                    boolean isFavorite,
                    long dueDate,
                    List<String> contacts) {
        this(name, description, isDone, isFavorite, dueDate);
        this.contacts = contacts;
    }

    public TodoItem() {
        this("", "", false, false, Calendar.getInstance().getTimeInMillis());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public long getDueDate() {
        return dueDate;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }

    public void addContact(String contactURI) {
        this.contacts.add(contactURI);
    }

    public void removeContact(String contactURI) {
        this.contacts.remove(contactURI);
    }

    public String getFormattedDateTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.dueDate);
        Date date = calendar.getTime();
        return formatter.format(date);
    }

    public String getFormattedDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.dueDate);
        Date date = calendar.getTime();
        return formatter.format(date);
    }

    public String getFormattedTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.GERMANY);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.dueDate);
        Date date = calendar.getTime();
        return formatter.format(date);
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    public String toString() {
        return name + " - "
                + description + " - "
                + isDone + " - "
                + isFavorite + " - "
                + getFormattedDateTime();
    }
}
