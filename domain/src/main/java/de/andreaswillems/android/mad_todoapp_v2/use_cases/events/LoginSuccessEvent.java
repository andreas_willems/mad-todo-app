package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class LoginSuccessEvent extends Event {

    public LoginSuccessEvent(String message) {
        super(message);
    }

}
