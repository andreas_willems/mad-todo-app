package de.andreaswillems.android.mad_todoapp_v2.use_cases;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.HideDoneItemsEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.OnlyFavouriteItemsEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoListEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.SortByDueDateEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.SortByFavoriteEvent;

public class SortFilterListOfTodoItemsUseCase {

    private static SortFilterListOfTodoItemsUseCase instance;
    private static boolean hideDone = true;
    private static boolean onlyFavourites = false;
    private static boolean sortByDueDate = true;
    private static boolean sortByFavorite = false;

    private SortFilterListOfTodoItemsUseCase() {
        EventBus.getDefault().register(this);
    }

    public static SortFilterListOfTodoItemsUseCase getInstance() {
        if (null == instance) {
            instance = new SortFilterListOfTodoItemsUseCase();
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onHideDoneItemsRequest(HideDoneItemsEvent event) {
        if (event.isHideDone()) {
            Log.i("SortListUseCase", "excluding done items...");
            hideDone = true;
        } else {
            Log.i("SortListUseCase", "including done items...");
            hideDone = false;
        }
        EventBus.getDefault().post(new RequestTodoListEvent("update todo list"));
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onOnlyFavouriteItemsRequest(OnlyFavouriteItemsEvent event) {
        if (event.isOnlyFavourite()) {
            Log.i("SortListUseCase", "exclude non favourite items");
            onlyFavourites = true;
        } else {
            Log.i("SortListUseCase", "include non favourite items");
            onlyFavourites = false;
        }
        EventBus.getDefault().post(new RequestTodoListEvent("update todo list"));
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onSortByDueDate(SortByDueDateEvent event) {
        sortByDueDate = true;
        sortByFavorite = false;
        EventBus.getDefault().post(new RequestTodoListEvent("update todo list"));
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onSortByFavorite(SortByFavoriteEvent event) {
        sortByDueDate = false;
        sortByFavorite = true;
        EventBus.getDefault().post(new RequestTodoListEvent("update todo list"));
    }

    List<TodoItem> sendThroughFilters(List<TodoItem> todoItems) {
        List<TodoItem> filteredList = new ArrayList<>(todoItems);

        // filter list
        if (hideDone) {
            filteredList = filterDoneItems(filteredList);
        }

        if (onlyFavourites) {
            filteredList = filterNonFavourites(filteredList);
        }

        return filteredList;
    }

    private List<TodoItem> filterDoneItems(List<TodoItem> todoItems) {
        List<TodoItem> filteredList = new ArrayList<>();
        for (TodoItem item : todoItems) {
            if (!item.isDone()) {
                filteredList.add(item);
            }
        }
        return filteredList;
    }

    private List<TodoItem> filterNonFavourites(List<TodoItem> todoItems) {
        List<TodoItem> filteredList = new ArrayList<>();
        for (TodoItem item : todoItems) {
            if (item.isFavorite()) {
                filteredList.add(item);
            }
        }
        return filteredList;
    }

    List<TodoItem> sendThroughSorters(List<TodoItem> todoItems) {
        List<TodoItem> sortedList = new ArrayList<>(todoItems);

        if (sortByDueDate) {
            sortedList = sortListByDueDate(sortedList);
        }

        if (sortByFavorite) {
            sortedList = sortListByFavourite(todoItems);
        }

        return sortedList;
    }

    private List<TodoItem> sortListByDueDate(List<TodoItem> todoItems) {
        Collections.sort(todoItems, new TodoItemChainedComparator(
                new TodoItemDueDateComparator(),
                new TodoItemDoneComparator()
        ));
        return todoItems;
    }

    private List<TodoItem> sortListByFavourite(List<TodoItem> todoItems) {
        Collections.sort(todoItems, new TodoItemChainedComparator(
                new TodoItemFavouriteComparator(),
                new TodoItemDueDateComparator(),
                new TodoItemDoneComparator()
        ));
        return todoItems;
    }

    private class TodoItemDueDateComparator implements Comparator<TodoItem> {

        @Override
        public int compare(TodoItem item1, TodoItem item2) {
            return Long.compare(item1.getDueDate(), item2.getDueDate());
        }
    }

    private class TodoItemFavouriteComparator implements Comparator<TodoItem> {

        @Override
        public int compare(TodoItem item1, TodoItem item2) {
            return Boolean.compare(item1.isFavorite(), item2.isFavorite());
        }
    }

    private class TodoItemDoneComparator implements Comparator<TodoItem> {
        @Override
        public int compare(TodoItem item1, TodoItem item2) {
            return Boolean.compare(item1.isDone(), item2.isDone());
        }
    }

    private class TodoItemChainedComparator implements Comparator<TodoItem> {

        private List<Comparator<TodoItem>> comparators;

        @SafeVarargs
        TodoItemChainedComparator(Comparator<TodoItem>... comparators) {
            this.comparators = Arrays.asList(comparators);
        }

        @Override
        public int compare(TodoItem item1, TodoItem item2) {
            for (Comparator<TodoItem> comparator : comparators) {
                int result = comparator.compare(item1, item2);
                if (0 != result) {
                    return result;
                }
            }
            return 0;
        }
    }
}