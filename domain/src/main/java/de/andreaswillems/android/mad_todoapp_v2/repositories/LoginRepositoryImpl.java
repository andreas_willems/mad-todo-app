package de.andreaswillems.android.mad_todoapp_v2.repositories;

import android.content.Context;

import de.andreaswillems.android.mad_todoapp_v2.mapping_entities.Credentials;
import de.andreaswillems.android.mad_todoapp_v2.network.LoginRestOperationsImpl;


public class LoginRepositoryImpl implements LoginRepository {

    private LoginRestOperationsImpl restOperations;

    public LoginRepositoryImpl(Context context) {
        this.restOperations = new LoginRestOperationsImpl();
    }

    @Override
    public boolean login(String email, String password) {
        return this.restOperations.requestLogin(new Credentials(email, password));
    }
}
