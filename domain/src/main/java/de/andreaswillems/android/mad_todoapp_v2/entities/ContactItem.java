package de.andreaswillems.android.mad_todoapp_v2.entities;


public class ContactItem {

    private String uri;
    private String id;
    private String name;
    private String number;
    private String emailAddress;

    public ContactItem(String uri) {
        this(uri, "", "", "", "");
    }

    public ContactItem(String uri, String id, String name, String number, String emailAddress) {
        this.uri = uri;
        this.id = id;
        this.name = name;
        this.number = number;
        this.emailAddress = emailAddress;
    }

    public String getUri() {
        return uri;
    }

    public String getTrimmedURI() {
        return this.uri.replace("[", "").replace("]", "").replace("\"", "");
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
