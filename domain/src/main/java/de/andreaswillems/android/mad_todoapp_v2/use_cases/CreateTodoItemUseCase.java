package de.andreaswillems.android.mad_todoapp_v2.use_cases;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.network.DataItemsRestApiOperationsImpl;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.CreateTodoItemEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemCreatedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemCreationFailedEvent;

public class CreateTodoItemUseCase {

    private static CreateTodoItemUseCase instance;
    private TodoListRepository todoListRepository;

    private CreateTodoItemUseCase(Context context) {
        todoListRepository = new TodoListRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static CreateTodoItemUseCase getInstance(Context context) {
        if (null == instance) {
            instance = new CreateTodoItemUseCase(context);
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onCreateTodoItemEvent(CreateTodoItemEvent event) {
        TodoItem todoItem = event.getTodoItem();
        Log.i("CreateTodoItem", String.valueOf(todoItem.getContacts().size()));
        long id = todoListRepository.saveItem(todoItem);
        if (id > -1) {
            todoItem.setId(id);
            boolean success = todoListRepository.saveRemoteItem(todoItem);
            if (success) {
                publishSuccess("Item saved");
            } else {
                publishError("Item saved only locally.");
            }
        } else {
            publishError("Saving item failed");
        }
    }

    private void publishSuccess(String message) {
        EventBus.getDefault().post(new TodoItemCreatedEvent(message));
    }

    private void publishError(String message) {
        EventBus.getDefault().post(
                new TodoItemCreationFailedEvent(message));
    }
}
