package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class SortByDueDateEvent extends Event {

    public SortByDueDateEvent(String message) {
        super(message);
    }

}
