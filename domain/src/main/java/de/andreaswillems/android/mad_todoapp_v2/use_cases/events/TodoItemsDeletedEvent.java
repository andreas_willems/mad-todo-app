package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemsDeletedEvent extends Event {

    public TodoItemsDeletedEvent(String message) {
        super(message);
    }

}
