package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class WebserviceAvailableRequestEvent extends Event {

    public WebserviceAvailableRequestEvent(String message) {
        super(message);
    }
}
