package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class RequestTodoListEvent extends Event {

    public RequestTodoListEvent(String message) {
        super(message);
    }

}
