package de.andreaswillems.android.mad_todoapp_v2.use_cases;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoListEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoListUpdatedEvent;

public class ShowListOfTodoItemsUseCase {

    private TodoListRepository todoListRepository;
    private SortFilterListOfTodoItemsUseCase sortWorker;
    private List<TodoItem> currentList;

    public ShowListOfTodoItemsUseCase(Context applicationContext) {
        todoListRepository = new TodoListRepositoryImpl(applicationContext);
        sortWorker = SortFilterListOfTodoItemsUseCase.getInstance();
        EventBus.getDefault().register(this);
    }

    private void publishResult(String message, List<TodoItem> list) {
        currentList = new ArrayList<>(list);

        // filter list
        currentList = sortWorker.sendThroughFilters(currentList);
        // sort list
        currentList = sortWorker.sendThroughSorters(currentList);

        EventBus.getDefault().post(new TodoListUpdatedEvent(
                message,
                currentList
        ));
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onTodoListRequest(RequestTodoListEvent event) {
        // initialize todolist in case everything fails
        List<TodoItem> todoItemList = new ArrayList<>();
        // check for online status
        if (LoginAtWebserviceUseCase.isWebserviceAvailable()) {
            Log.i("ShowListUseCase", "trying online...");

            int numberOfItems = todoListRepository.getItemCount();
            if (numberOfItems > 0) {
                // delete remote items and sync local items
                clearItemsInWebservice();
                todoItemList = todoListRepository.getItems();
                syncLocalItemsToWebservice(todoItemList);

            } else {
                // load remote items and persist them
                todoItemList = loadItemsFromWebservice();
                todoListRepository.saveListOfItems(todoItemList);
            }
            todoItemList = loadItemsFromWebservice();
        } else {
            // load offline list
            Log.i("ShowListUseCase", "working offline...");
            todoItemList = todoListRepository.getItems();
        }

        publishResult("fetched todo list", todoItemList);
    }


    private void clearItemsInWebservice() {
        todoListRepository.deleteRemoteItems();
    }

    private void syncLocalItemsToWebservice(List<TodoItem> todoItems) {
        todoListRepository.saveListOfItems(todoItems);
    }

    private List<TodoItem> loadItemsFromWebservice() {
        return todoListRepository.getItemsRemote();
    }

}
