package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class RequestLoginEvent extends Event {

    private String message;
    private String email;
    private String password;

    public RequestLoginEvent(String message, String email, String password) {
        super(message);
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
