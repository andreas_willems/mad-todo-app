package de.andreaswillems.android.mad_todoapp_v2.repositories;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;

/**
 * Defines operations to store and access TodoItems in form of a list.
 *
 * @author Andreas Willems
 * @version 28 JUN 2017
 */

public interface TodoListRepository {

    /**
     * Saves an item created in the app.
     */
    public long saveItem(TodoItem todoItem);


    /**
     * Sends the created todoitem to the remote server.
     */
    public boolean saveRemoteItem(TodoItem todoItem);

    /**
     * Saves a list of items, for example fetched from an external store.
     */
    public void saveListOfItems(List<TodoItem> listOfItems);

    /**
     * Gets the items stored.
     */
    public List<TodoItem> getItems();

    /**
     * Gets all items from the remote webservice.
     */
    public List<TodoItem> getItemsRemote();

    /**
     * Gets the item with the given id.
     */
    public TodoItem getItem(long id);

    /**
     * Deletes the item permanently.
     */
    public boolean deleteItem(long id);


    /**
     * Deletes the remote item permanently.
     */
    public boolean deleteRemoteItem(long id);

    /**
     * Updates the persisted item with the parameters of the given item.
     */
    public boolean updateItem(TodoItem updatedItem);

    /**
     * Updates the corresponding remote item
     */
    public boolean updateRemoteItem(TodoItem todoItem);

    /**
     * Returns the number of items in the repository.
     */
    public int getItemCount();

    /**
     * Deletes all items stored in the webservice.
     */
    public boolean deleteRemoteItems();

    /**
     * Deletes all items stored on the device.
     */
    public boolean deleteLocalItems();
}
