package de.andreaswillems.android.mad_todoapp_v2.repositories;

/**
 * Defines operatons to provide user authentication against a webservice.
 *
 * @author Andreas Willems
 * @version 29 JUN 2017
 */

public interface LoginRepository {

    /**
     * Sends a login request to a webservice and returns true,
     * if successful, otherwise false;
     */
    public boolean login(String name, String password);
}
