package de.andreaswillems.android.mad_todoapp_v2.use_cases;


import android.content.Context;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.repositories.ContactRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.ContactRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactFetchFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactFetchedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.ContactListFetchedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestContactDataEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestContactsForTodoEvent;

public class ReadContactDataUseCase {

    private static ReadContactDataUseCase instance;
    private ContactRepository contactRepository;

    private ReadContactDataUseCase(Context context) {
        contactRepository = new ContactRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static ReadContactDataUseCase getInstance(Context context) {
        if(null == instance) {
            instance = new ReadContactDataUseCase(context);
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onContactDataRequest(RequestContactDataEvent event) {
        ContactItem contact = contactRepository.getContact(event.getContactUri());
        if (null != contact) {
            publishSuccess("Contact data successfully fetched.", contact);
        } else {
            publishError("Fetching of contact data failed.");
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onContactDataForTodoRequest(RequestContactsForTodoEvent event) {
        TodoItem todoItem = event.getTodoItem();
        List<String> uriStrings = todoItem.getContacts();
        if (uriStrings.size() > 0) {
            List<ContactItem> contacts =
                    contactRepository.getContactsFromURIStrings(uriStrings);
            if (null != contacts) {
                publishSuccess("Contacts successfully fetched.", contacts);
            } else {
                publishError("Fetching contacts failed.");
            }
        }

    }

    private void publishSuccess(String message, ContactItem contact) {
        EventBus.getDefault().post(new ContactFetchedEvent(message, contact));
    }

    private void publishSuccess(String message, List<ContactItem> contacts) {
        EventBus.getDefault().post(new ContactListFetchedEvent(message, contacts));
    }

    private void publishError(String message) {
        EventBus.getDefault().post(new ContactFetchFailedEvent(message));
    }

}
