package de.andreaswillems.android.mad_todoapp_v2.use_cases;


import android.content.Context;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.DeleteAllItemsRequestEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemsDeletionFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoListUpdatedEvent;

public class DeleteAllTodoItemsUseCase {

    private static DeleteAllTodoItemsUseCase instance;
    private TodoListRepository todoListRepository;

    private DeleteAllTodoItemsUseCase(Context context) {
        todoListRepository = new TodoListRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static DeleteAllTodoItemsUseCase getInstance(Context context) {
        if (null == instance) {
            instance = new DeleteAllTodoItemsUseCase(context);
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onDeleteAllTodoItemsRequest(DeleteAllItemsRequestEvent event) {
        boolean localSuccess = todoListRepository.deleteLocalItems();
        if (localSuccess) {
            /*
            boolean remoteSuccess = todoListRepository.deleteRemoteItems();
            if (remoteSuccess) {
                publishSuccess("All items deleted.");
            } else {
                publishSuccess("Only local items deleted.");
            }
            */
            publishSuccess("All items deleted");
        } else {
            publishError("Deleting items failed.");
        }
    }

    private void publishSuccess(String message) {
        EventBus.getDefault().post(new TodoListUpdatedEvent(
                message, new ArrayList<TodoItem>())
        );
    }

    private void publishError(String message) {
        EventBus.getDefault().post(new TodoItemsDeletionFailedEvent(message));
    }
}
