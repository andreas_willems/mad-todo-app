package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemDeleteFailedEvent extends Event {

    public TodoItemDeleteFailedEvent(String message) {
        super(message);
    }

}
