package de.andreaswillems.android.mad_todoapp_v2.use_cases;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemDeleteEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemDeleteFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemDeletedEvent;

public class DeleteTodoItemUseCase {

    private static DeleteTodoItemUseCase instance;
    private TodoListRepository todoListRepository;

    private DeleteTodoItemUseCase(Context context) {
        todoListRepository = new TodoListRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static DeleteTodoItemUseCase getInstance(Context context) {
        if (null == instance) {
            instance = new DeleteTodoItemUseCase(context);
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onDeleteItemRequest(RequestTodoItemDeleteEvent event) {
        Log.i("request delete", event.getMessage());
        boolean deleted = todoListRepository.deleteItem(
                event.getTodoItem().getId()
        );
        if (deleted) {
            publishSuccess("item deleted");
        } else {
            publishError("deletion failed");
        }

    }

    private void publishSuccess(String message) {
        EventBus.getDefault().post(new TodoItemDeletedEvent(message));
    }

    private void publishError(String message) {
        EventBus.getDefault().post(new TodoItemDeleteFailedEvent(message));
    }
}
