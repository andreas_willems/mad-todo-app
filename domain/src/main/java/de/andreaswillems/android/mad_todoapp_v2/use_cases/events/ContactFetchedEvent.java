package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;


public class ContactFetchedEvent extends Event {

    private ContactItem contact;

    public ContactFetchedEvent(String message, ContactItem contact) {
        super(message);
        this.contact = contact;
    }

    public ContactItem getContact() {
        return contact;
    }
}
