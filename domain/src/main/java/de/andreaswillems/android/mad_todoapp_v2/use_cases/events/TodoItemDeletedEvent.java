package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemDeletedEvent extends Event {

    public TodoItemDeletedEvent(String message) {
        super(message);
    }

}
