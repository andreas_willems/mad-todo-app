package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;



public class RequestTodoItemEvent extends Event {

    private long itemId;

    public RequestTodoItemEvent(String message, long itemId) {
        super(message);
        this.itemId = itemId;
    }


    public long getItemId() {
        return itemId;
    }
}
