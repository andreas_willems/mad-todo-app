package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class SortByFavoriteEvent extends Event {

    public SortByFavoriteEvent(String message) {
        super(message);
    }

}

