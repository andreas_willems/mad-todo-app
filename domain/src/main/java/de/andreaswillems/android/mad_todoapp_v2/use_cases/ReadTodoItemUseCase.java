package de.andreaswillems.android.mad_todoapp_v2.use_cases;


import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.repositories.ContactRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.ContactRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemFetchedEvent;

public class ReadTodoItemUseCase {

    private static ReadTodoItemUseCase instance;
    private TodoListRepository todoListRepository;
    private ContactRepository contactRepository;

    private  ReadTodoItemUseCase(Context context) {
        todoListRepository = new TodoListRepositoryImpl(context);
        contactRepository = new ContactRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static ReadTodoItemUseCase getInstance(Context context) {
        if (null == instance) {
            instance = new ReadTodoItemUseCase(context);
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onTodoItemRequest(RequestTodoItemEvent event) {
        // Log.i("fetch item request", Long.toString(event.getItemId()));
        TodoItem todoItem = todoListRepository.getItem(event.getItemId());
        if (null != todoItem) {
            publishSuccess("Item fetched", todoItem);
        } else {
            publishError("fetching failed");
        }
    }

    /*private void appendContactInfo(TodoItem todoItem) {
        List<ContactItem> contactURIs = todoItem.getContacts();
        List<ContactItem> contacts = contactRepository
                .inflateContactList(contactURIs);
        todoItem.setContacts(contacts);
        publishSuccess("item fetched", todoItem);
    }*/

    private void publishSuccess(String message, TodoItem todoItem) {
        EventBus.getDefault().post(new TodoItemFetchedEvent(message, todoItem));
    }

    private void publishError(String message) {
        EventBus.getDefault().post(new TodoItemFetchFailedEvent(message));
    }
}
