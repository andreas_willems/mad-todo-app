package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemFetchFailedEvent extends Event {

    public TodoItemFetchFailedEvent(String message) {
        super(message);
    }

}
