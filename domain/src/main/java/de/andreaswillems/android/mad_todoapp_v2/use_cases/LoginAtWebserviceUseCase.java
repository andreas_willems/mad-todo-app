package de.andreaswillems.android.mad_todoapp_v2.use_cases;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.repositories.LoginRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.LoginRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.LoginErrorEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.LoginFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.LoginSuccessEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestLoginEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.WebserviceAvailableEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.WebserviceAvailableRequestEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.WebserviceUnavailableEvent;
import de.andreaswillems.android.mad_todoapp_v3.connection.NetworkConnection;

public class LoginAtWebserviceUseCase {

    private static LoginAtWebserviceUseCase instance;
    private static boolean isOnline;
    private NetworkConnection networkConnection;
    private LoginRepository loginRepository;

    private LoginAtWebserviceUseCase(Context context) {
        this.networkConnection = new NetworkConnection(context);
        this.loginRepository = new LoginRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static LoginAtWebserviceUseCase getInstance(Context context) {
        if (null == instance) {
            isOnline = false;
            instance = new LoginAtWebserviceUseCase(context);
        }
        return instance;
    }

    public static boolean isWebserviceAvailable() {
        return isOnline;
    }

    private boolean isLoginFeatureAvailable() {
        boolean connected = networkConnection.isConnectedToNetwork();
        Log.i("Login connected", String.valueOf(connected));
        boolean available = networkConnection.isWebserviceAvailable();
        Log.i("Login available", String.valueOf(available));
        return connected && available;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onWebserviceAvailableRequest(WebserviceAvailableRequestEvent event) {
        if (isLoginFeatureAvailable()) {
            isOnline = true;
            EventBus.getDefault().post(new WebserviceAvailableEvent(
                    "webservice available"
            ));
        } else {
            isOnline = false;
            EventBus.getDefault().post(new WebserviceUnavailableEvent(
                    "webservice not available"
            ));
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onLoginRequest(RequestLoginEvent event) {
        String email = event.getEmail();
        String password = event.getPassword();
        boolean result = this.loginRepository.login(email, password);
        if (result) {
            EventBus.getDefault().post(new LoginSuccessEvent("Login successful"));
        } else {
            EventBus.getDefault().post(new LoginFailedEvent("Login failed"));
        }
    }
}
