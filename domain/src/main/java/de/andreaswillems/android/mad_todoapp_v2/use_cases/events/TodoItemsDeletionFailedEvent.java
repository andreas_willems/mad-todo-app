package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemsDeletionFailedEvent extends Event {

    public TodoItemsDeletionFailedEvent(String message) {
        super(message);
    }

}
