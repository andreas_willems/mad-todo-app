package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public abstract class Event {

    private String message;

    public Event(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
