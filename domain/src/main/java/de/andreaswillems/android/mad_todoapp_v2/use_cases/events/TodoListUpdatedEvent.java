package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;

public class TodoListUpdatedEvent extends Event {

    private List<TodoItem> todoItemList;

    public TodoListUpdatedEvent(String message, List<TodoItem> todoItemList) {
        super(message);
        this.todoItemList = todoItemList;
    }

    public List<TodoItem> getTodoItemList() {
        return todoItemList;
    }
}
