package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;


public class ContactListFetchedEvent extends Event{

    private List<ContactItem> contacts;

    public ContactListFetchedEvent(String message, List<ContactItem> contacts) {
        super(message);
        this.contacts = contacts;
    }

    public List<ContactItem> getContacts() {
        return contacts;
    }
}
