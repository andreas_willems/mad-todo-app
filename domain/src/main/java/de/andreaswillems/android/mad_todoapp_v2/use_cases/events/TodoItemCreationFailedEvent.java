package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemCreationFailedEvent extends Event {

    private String message;

    public TodoItemCreationFailedEvent(String message) {
        super(message);
    }


}
