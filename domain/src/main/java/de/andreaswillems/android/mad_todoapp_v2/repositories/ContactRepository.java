package de.andreaswillems.android.mad_todoapp_v2.repositories;


import android.net.Uri;

import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;

public interface ContactRepository {

    public ContactItem getContact(Uri uri);

    public List<ContactItem> getContacts(List<Uri> uris);

    public List<ContactItem> getContactsFromURIStrings(List<String> uriStrings);
}
