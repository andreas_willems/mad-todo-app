package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class DeleteAllItemsRequestEvent extends Event {

    public DeleteAllItemsRequestEvent(String message) {
        super(message);
    }

}
