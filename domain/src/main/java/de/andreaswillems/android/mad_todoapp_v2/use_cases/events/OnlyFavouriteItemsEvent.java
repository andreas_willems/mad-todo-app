package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class OnlyFavouriteItemsEvent extends Event {

    public static final boolean EXCLUDE = false;
    public static final boolean INCLUDE = true;
    private boolean onlyFavourite;

    public OnlyFavouriteItemsEvent(String message, boolean onlyFavourite) {
        super(message);
        this.onlyFavourite = onlyFavourite;
    }

    public boolean isOnlyFavourite() {
        return onlyFavourite;
    }
}
