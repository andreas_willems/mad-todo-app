package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class WebserviceAvailableEvent extends Event {

    public WebserviceAvailableEvent(String message) {
        super(message);
    }

}
