package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class HideDoneItemsEvent extends Event {

    public static final boolean EXCLUDE = true;
    public static final boolean INCLUDE = false;
    private boolean hideDone;

    public HideDoneItemsEvent(String message, boolean hideDone) {
        super(message);
        this.hideDone = hideDone;
    }

    public boolean isHideDone() {
        return hideDone;
    }
}
