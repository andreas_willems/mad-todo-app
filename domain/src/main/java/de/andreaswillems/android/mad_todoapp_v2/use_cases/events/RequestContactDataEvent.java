package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


import android.net.Uri;

public class RequestContactDataEvent extends Event {

    private Uri contactUri;

    public RequestContactDataEvent(String message, Uri contactUri) {
        super(message);
        this.contactUri = contactUri;
    }

    public Uri getContactUri() {
        return contactUri;
    }
}
