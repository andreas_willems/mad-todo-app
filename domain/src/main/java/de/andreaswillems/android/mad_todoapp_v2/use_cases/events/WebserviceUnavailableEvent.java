package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class WebserviceUnavailableEvent extends Event {

    public WebserviceUnavailableEvent(String message) {
        super(message);
    }

}
