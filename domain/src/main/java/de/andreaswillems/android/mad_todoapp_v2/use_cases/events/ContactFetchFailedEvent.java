package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class ContactFetchFailedEvent extends Event {

    public ContactFetchFailedEvent(String message) {
        super(message);
    }

}
