package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class NetworkPermissionGrantedEvent extends Event {

    public NetworkPermissionGrantedEvent(String message) {
        super(message);
    }

}
