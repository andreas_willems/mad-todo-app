package de.andreaswillems.android.mad_todoapp_v2.repositories;

import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.andreaswillems.android.mad_todoapp_v2.entities.ContactItem;
import de.andreaswillems.android.mad_todoapp_v3.contacts.ReadContactData;


public class ContactRepositoryImpl implements ContactRepository {

    private ReadContactData readContactData;

    public ContactRepositoryImpl(Context context) {
        this.readContactData = new ReadContactData(context);
    }

    @Override
    public ContactItem getContact(Uri uri) {
        HashMap<String, String> contactData =
                this.readContactData.processContactData(uri);
        return mapToContactItem(contactData);
    }

    @Override
    public List<ContactItem> getContacts(List<Uri> uris) {
        List<ContactItem> contactList = new ArrayList<>();
        for (Uri uri : uris) {
            ContactItem contact = getContact(uri);
            contactList.add(contact);
        }
        return contactList;
    }

    @Override
    public List<ContactItem> getContactsFromURIStrings(List<String> uriStrings) {
        List<Uri> uris = new ArrayList<>();
        for (String uriString : uriStrings) {
            uris.add(Uri.parse(uriString));
        }
        return getContacts(uris);
    }

    private ContactItem mapToContactItem(HashMap<String, String> contactData) {
        if (isValidContactData(contactData)) {
            return new ContactItem(
                    contactData.get("uri"),
                    contactData.get(ContactsContract.Contacts._ID),
                    contactData.get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME),
                    contactData.get(ContactsContract.CommonDataKinds.Phone.NUMBER),
                    contactData.get(ContactsContract.CommonDataKinds.Email.ADDRESS)
            );
        } else {
            return null;
        }
    }

    private boolean isValidContactData(HashMap<String, String> contactData) {
        return contactData.containsKey(ContactsContract.Contacts._ID)
                && contactData.containsKey(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                && contactData.containsKey(ContactsContract.CommonDataKinds.Phone.NUMBER)
                && contactData.containsKey(ContactsContract.CommonDataKinds.Email.ADDRESS);
    }
}
