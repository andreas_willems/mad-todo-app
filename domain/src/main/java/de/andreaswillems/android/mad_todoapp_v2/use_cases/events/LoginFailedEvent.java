package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class LoginFailedEvent extends Event{

    public LoginFailedEvent(String message) {
        super(message);
    }

}
