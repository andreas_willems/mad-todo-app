package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;


public class CreateTodoItemEvent extends Event {

    private TodoItem todoItem;

    public CreateTodoItemEvent(String message, TodoItem todoItem) {
        super(message);
        this.todoItem = todoItem;
    }

    public TodoItem getTodoItem() {
        return todoItem;
    }
}
