package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class TodoItemCreatedEvent extends Event {

    public TodoItemCreatedEvent(String message) {
        super(message);
    }

}
