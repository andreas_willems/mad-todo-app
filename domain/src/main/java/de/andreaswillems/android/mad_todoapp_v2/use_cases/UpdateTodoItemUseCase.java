package de.andreaswillems.android.mad_todoapp_v2.use_cases;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.andreaswillems.android.mad_todoapp_v2.entities.TodoItem;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepository;
import de.andreaswillems.android.mad_todoapp_v2.repositories.TodoListRepositoryImpl;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.RequestTodoItemUpdateEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdateFailedEvent;
import de.andreaswillems.android.mad_todoapp_v2.use_cases.events.TodoItemUpdatedEvent;

public class UpdateTodoItemUseCase {

    private static UpdateTodoItemUseCase instance;
    private TodoListRepository todoListRepository;

    private UpdateTodoItemUseCase(Context context) {
        todoListRepository = new TodoListRepositoryImpl(context);
        EventBus.getDefault().register(this);
    }

    public static UpdateTodoItemUseCase getInstance(Context context) {
        if (null == instance) {
            instance = new UpdateTodoItemUseCase(context);
        }
        return instance;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onUpdateTodoItemRequest(RequestTodoItemUpdateEvent event) {
        Log.i("TodoItemUpdate", "requested...");
        boolean updated = todoListRepository.updateItem(event.getTodoItem());
        if (updated) {
            publishSuccess("local item updated", event.getTodoItem());
            boolean success = todoListRepository.updateRemoteItem(event.getTodoItem());
            if (success) {
                Log.i("TodoItemUpdate", "remote update successful...");
                publishSuccess("item updated", event.getTodoItem());
            } else {
                Log.i("TodoItemUpdate", "remote update failed...");
                publishError("remote item update failed", event.getTodoItem());
            }
        } else {
            Log.i("TodoItemUpdate", "local update failed...");
            publishError("item update failed", event.getTodoItem());
        }
    }

    private void publishSuccess(String message, TodoItem todoItem) {
        EventBus.getDefault().post(new TodoItemUpdatedEvent(
                message,
                todoItem
        ));
    }

    private void publishError(String message, TodoItem todoItem){
        EventBus.getDefault().post(new TodoItemUpdateFailedEvent(
                message,
                todoItem
        ));
    }
}
