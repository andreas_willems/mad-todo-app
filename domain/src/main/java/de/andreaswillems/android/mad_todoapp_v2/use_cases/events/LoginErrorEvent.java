package de.andreaswillems.android.mad_todoapp_v2.use_cases.events;


public class LoginErrorEvent extends Event {

    public LoginErrorEvent(String message) {
        super(message);
    }

}
